package be.daStudios.creatures;

import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.armour.Armour;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.utilities.Dices;

public class Monster implements Creature {
    MonsterRace race;
    int xp;
    private int speed;
    private int hitPoints;
    private Weapon weapon;
    private AdditionalArmour secondWeapon;
    private Armour armour;
    private int gold;
    private int totalArmour;
    private int attackModifier;
    private Dices damageDice;
    private int damageModifier;
    private boolean key;

    public Monster(MonsterRace race) {
        setRace(race);
        applyAllRacemodifiers();
    }

    @Override
    public void attack (Creature target) {
        int totalAttack = Dices.d20.getValue() + getAttackModifier();
        int totalDefense = target.getTotalArmour();
        int damage = 0;
        if (totalAttack <= totalDefense) {
            System.out.println("The monsters attack failed!");
        } else {
            damage = getDamageDice().getValue() + getDamageModifier();
            System.out.println("The monster did " + damage + " damage!");
            target.setHitPoints(target.getHitPoints() - damage);
            System.out.println("Its opponent has " + target.getHitPoints() + " hitpoints left!");
            if (target.getHitPoints() <= 0 ) {
                System.out.println("The monster has killed its opponent!");
            }
        }
    }


//    ApplyModifiers

    public void applyAllRacemodifiers () {
        setXp(race.getXp());
        setSpeed(race.getSpeed());
        setHitPoints(race.getHitPoints());
        setWeapon(race.getWeapon());
        setSecondWeapon(race.getSecondWeapon());
        setArmour(race.getArmour());
        setGold(race.getStartGold());
        setTotalArmour(race.getTotalArmour());
        setAttackModifier(race.getAttackModifier());
        setDamageDice(race.getDamageDice());
        setDamageModifier(race.getDamageModifier());
        setKey(race.isKey());
    }

    //Generate Random Goblins

    public static Monster generateAndReturnRandomGoblin() {
        int monsterchoice = Dices.d4.getValue();
        Monster monster = null;
        switch (monsterchoice) {
            case 1:
                monster = new Monster(MonsterRace.GoblinMinion);
                break;
            case 2:
                monster = new Monster(MonsterRace.GoblinFighter);
                break;
            case 3:
                monster = new Monster(MonsterRace.GoblinRanger);
                break;
            case 4:
                monster = new Monster(MonsterRace.Hobgoblin);
                break;
        }
        return monster;
    }

//    Getters and Setters


    public Races getRace() {
        return race;
    }

    public void setRace(MonsterRace race) {
        this.race = race;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }
    @Override
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getHitPoints() {
        return hitPoints;
    }

    @Override
    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public AdditionalArmour getSecondWeapon() {
        return secondWeapon;
    }

    public void setSecondWeapon(AdditionalArmour secondWeapon) {
        this.secondWeapon = secondWeapon;
    }

    public Armour getArmour() {
        return armour;
    }

    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    @Override
    public int getTotalArmour() {
        return totalArmour;
    }

    public void setTotalArmour(int totalArmour) {
        this.totalArmour = totalArmour;
    }

    public int getAttackModifier() {
        return attackModifier;
    }

    public void setAttackModifier(int attackModifier) {
        this.attackModifier = attackModifier;
    }

    public Dices getDamageDice() {
        return damageDice;
    }

    public void setDamageDice(Dices damageDice) {
        this.damageDice = damageDice;
    }

    public int getDamageModifier() {
        return damageModifier;
    }

    public void setDamageModifier(int damageModifier) {
        this.damageModifier = damageModifier;
    }
    @Override
    public boolean isKey() {
        return key;
    }
    @Override
    public void setKey(boolean key) {
        this.key = key;
    }
}
