package be.daStudios.creatures;

import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.armour.Armour;
import be.daStudios.armour.LeatherArmour;
import be.daStudios.armour.additionalArmour.Shield;
import be.daStudios.equippable.weapons.*;
import be.daStudios.utilities.Dices;

public enum MonsterRace implements Races{
    GoblinMinion (10, 3, 1, new Sword(), null, new LeatherArmour(), 5, 13, 3, Dices.d4, 2, 0, false),
    GoblinRanger (100, 3, 12, new Bow(), null, new LeatherArmour(), 5, 15, 4, Dices.d6, 3, 0, false),
    GoblinFighter (100,3, 3, new Sword(), new Shield(), new LeatherArmour(), 5, 17, 4, Dices.d6, 3, 0, false),
    Hobgoblin (150, 3, 25, new Sword(), null, new LeatherArmour(), 5, 18, 4, Dices.d8, 3, 0, false),
    Wolf (50, 6, 40, new Bite(), null, null, 0, 19, 3, Dices.d6, 3, 0, false),
    Bugbear(450,3,34,new Sword(),null,null,5,18,5,Dices.d10,4,1, true),
    Troll(800,3,40,new Club(),null,new LeatherArmour(),0,19,5,Dices.d10,4,2, true);


    private int xp;
    private int speed;
    private int hitPoints;
    private Weapon weapon;
    private AdditionalArmour secondWeapon;
    private Armour armour;
    private int startGold;
    private int totalArmour;
    private int AttackModifier;
    private Dices damageDice;
    private int DamageModifier;
    private int specialAttack;
    private boolean key;

    MonsterRace (int xp,
                 int speed,
                 int hitPoints,
                 Weapon weapon,
                 AdditionalArmour secondWeapon,
                 Armour armour,
                 int startGold,
                 int totalArmour,
                 int attackModifier,
                 Dices damageDice,
                 int damageModifier,
                 int specialAttack,
                 boolean key) {
        setXp(xp);
        setSpeed(speed);
        setHitPoints(hitPoints);
        setWeapon(weapon);
        setSecondWeapon(secondWeapon);
        setArmour(armour);
        setStartGold(startGold);
        setTotalArmour(totalArmour);
        setAttackModifier(attackModifier);
        setDamageDice(damageDice);
        setDamageModifier(damageModifier);
        setSpecialAttack(specialAttack);
        setKey(key);
    }


    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public AdditionalArmour getSecondWeapon() {
        return secondWeapon;
    }

    public void setSecondWeapon(AdditionalArmour secondWeapon) {
        this.secondWeapon = secondWeapon;
    }

    public Armour getArmour() {
        return armour;
    }

    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    public int getStartGold() {
        return startGold;
    }

    public void setStartGold(int startGold) {
        this.startGold = startGold;
    }

    public int getTotalArmour() {
        return totalArmour;
    }

    public void setTotalArmour(int totalArmour) {
        this.totalArmour = totalArmour;
    }

    public int getAttackModifier() {
        return AttackModifier;
    }

    public void setAttackModifier(int attackModifier) {
        AttackModifier = attackModifier;
    }

    public Dices getDamageDice() {
        return damageDice;
    }

    public void setDamageDice(Dices damageDice) {
        this.damageDice = damageDice;
    }

    public int getDamageModifier() {
        return DamageModifier;
    }

    public void setDamageModifier(int damageModifier) {
        DamageModifier = damageModifier;
    }

    public int getSpecialAttack() {
        return specialAttack;
    }

    public void setSpecialAttack(int specialAttack) {
        this.specialAttack = specialAttack;
    }

    public boolean isKey() {
        return key;
    }

    public void setKey(boolean key) {
        this.key = key;
    }
}
