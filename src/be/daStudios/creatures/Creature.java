package be.daStudios.creatures;

import java.io.Serializable;

public interface Creature extends Serializable {
    void attack (Creature target);
    Races getRace();
    int getTotalArmour();
    int getHitPoints();
    void setHitPoints(int hitPoints);
    int getSpeed();
    boolean isKey();
    void setKey(boolean Key);
}
