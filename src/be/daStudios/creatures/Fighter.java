package be.daStudios.creatures;

import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.armour.Armour;
import be.daStudios.armour.ChainMail;
import be.daStudios.backpack.BackPack;
import be.daStudios.backpack.Pack;
import be.daStudios.armour.additionalArmour.Shield;
import be.daStudios.equippable.weapons.Sword;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.utilities.Dices;


public class Fighter extends PlayableCreature {


    public Fighter (Race race) {
        setRace(race);
        setLevel(0);
        setXp(0);
        setHitPoints(Dices.d12.getValue());
        setAbilities(new Abilities (0,18,15,14,13,11,8, 8));
        setCombatModifiers(new CombatModifiers(0,0,0,0,0,0,0,0,
                0,0,0,0,0,0));
        setPack(new BackPack());
        getPack().addGold(15);
        setWeapon(new Sword());
        setShield(new Shield());
        setArmour(new ChainMail());
        applyAllRaceModifiers();
        applyAllCombatModifiers();
        setKey(false);
    }

}
