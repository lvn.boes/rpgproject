package be.daStudios.creatures;

import be.daStudios.armour.*;
import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.backpack.BackPack;
import be.daStudios.backpack.Pack;
import be.daStudios.equippable.weapons.Bow;
import be.daStudios.equippable.weapons.Sword;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.utilities.Dices;

public class Ranger extends PlayableCreature{

    public Ranger(Race race) {
        setRace(race);
        setLevel(1);
        setHitPoints(Dices.d8.getValue());
        setAbilities(new Abilities (0,8,15,13,11,18,14, 6));
        setCombatModifiers(new CombatModifiers(0,0,0,0,0,0,0,0,
                0,0,0,0,0,0));
        setPack(new BackPack());
        getPack().addGold(10);
        setWeapon(new Bow());
        getPack().addItem(new Sword());
        setArmour(new LeatherArmour());
        applyAllRaceModifiers();
        applyAllCombatModifiers();
        setKey(false);
    }

}
