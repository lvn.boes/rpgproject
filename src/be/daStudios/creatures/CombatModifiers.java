package be.daStudios.creatures;

import java.io.Serializable;

public class CombatModifiers implements Serializable {
    private int proficiency;
    private int totalArmour;
    private int strengthModifier;
    private int constitutionModifier;
    private int wisdomModifier;
    private int charismaModifier;
    private int dexterityModifier;
    private int intelligenceModifier;
    private int swordAttackModifier;
    private int swordDamageModifier;
    private int bowAttackModifier;
    private int bowDamageModifier;
    private int curseAttackModifier;
    private int curseDamageModifier;


    public CombatModifiers(int proficiency, int totalArmour, int strengthModifier, int constitutionModifier,
                           int wisdomModifier, int charismaModifier, int dexterityModifier, int intelligenceModifier,
                           int swordAttackModifier, int swordDamageModifier, int bowAttackModifier, int bowDamageModifier,
                           int curseAttackModifier, int curseDamageModifier) {
        this.proficiency = proficiency;
        this.totalArmour = totalArmour;
        this.strengthModifier = strengthModifier;
        this.constitutionModifier = constitutionModifier;
        this.wisdomModifier = wisdomModifier;
        this.charismaModifier = charismaModifier;
        this.dexterityModifier = dexterityModifier;
        this.intelligenceModifier = intelligenceModifier;
        this.swordAttackModifier = swordAttackModifier;
        this.swordDamageModifier = swordDamageModifier;
        this.bowAttackModifier = bowAttackModifier;
        this.bowDamageModifier = bowDamageModifier;
        this.curseAttackModifier = curseAttackModifier;
        this.curseDamageModifier = curseDamageModifier;
    }



    public int getProficiency() {
        return proficiency;
    }

    public void setProficiency(int proficiency) {
        this.proficiency = proficiency;
    }

    public int getTotalArmour() {
        return totalArmour;
    }

    public void setTotalArmour(int totalArmour) {
        this.totalArmour = totalArmour;
    }

    public int getStrengthModifier() {
        return strengthModifier;
    }

    public void setStrengthModifier(int strengthModifier) {
        this.strengthModifier = strengthModifier;
    }

    public int getConstitutionModifier() {
        return constitutionModifier;
    }

    public void setConstitutionModifier(int constitutionModifier) {
        this.constitutionModifier = constitutionModifier;
    }

    public int getWisdomModifier() {
        return wisdomModifier;
    }

    public void setWisdomModifier(int wisdomModifier) {
        this.wisdomModifier = wisdomModifier;
    }

    public int getCharismaModifier() {
        return charismaModifier;
    }

    public void setCharismaModifier(int charismaModifier) {
        this.charismaModifier = charismaModifier;
    }

    public int getDexterityModifier() {
        return dexterityModifier;
    }

    public void setDexterityModifier(int dexterityModifier) {
        this.dexterityModifier = dexterityModifier;
    }

    public int getIntelligenceModifier() {
        return intelligenceModifier;
    }

    public void setIntelligenceModifier(int intelligenceModifier) {
        this.intelligenceModifier = intelligenceModifier;
    }

    public int getSwordAttackModifier() {
        return swordAttackModifier;
    }

    public void setSwordAttackModifier(int swordAttackModifier) {
        this.swordAttackModifier = swordAttackModifier;
    }

    public int getSwordDamageModifier() {
        return swordDamageModifier;
    }

    public void setSwordDamageModifier(int swordDamageModifier) {
        this.swordDamageModifier = swordDamageModifier;
    }

    public int getBowAttackModifier() {
        return bowAttackModifier;
    }

    public void setBowAttackModifier(int bowAttackModifier) {
        this.bowAttackModifier = bowAttackModifier;
    }

    public int getBowDamageModifier() {
        return bowDamageModifier;
    }

    public void setBowDamageModifier(int bowDamageModifier) {
        this.bowDamageModifier = bowDamageModifier;
    }

    public int getCurseAttackModifier() {
        return curseAttackModifier;
    }

    public void setCurseAttackModifier(int curseAttackModifier) {
        this.curseAttackModifier = curseAttackModifier;
    }

    public int getCurseDamageModifier() {
        return curseDamageModifier;
    }

    public void setCurseDamageModifier(int curseDamageModifier) {
        this.curseDamageModifier = curseDamageModifier;
    }
}
