package be.daStudios.creatures;

import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.armour.Armour;
import be.daStudios.backpack.Pack;
import be.daStudios.equippable.weapons.Bow;
import be.daStudios.equippable.weapons.HolySymbol;
import be.daStudios.equippable.weapons.Sword;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.utilities.Dices;
import be.daStudios.utilities.Keyboard;
import be.daStudios.utilities.Utilities;

import java.util.List;

public abstract class PlayableCreature implements Creature{
    Race race;
    int level;
    int xp;
    private int hitPoints;
    private Abilities abilities;
    private CombatModifiers combatModifiers;
    private Pack pack;
    private Weapon weapon;
    private AdditionalArmour shield;
    private Armour armour;
    private int totalSwordAttack;
    private int totalBowAttack;
    private int totalHolySymbolAttack;
    private int totalSwordDamage;
    private int totalBowDamage;
    private int totalHolySymbolDamage;
    private List<String> attackStrings;
    private boolean key;

//    Attack

    @Override
    public void attack (Creature target) {
        normalAttack(target);
//        if (getLevel() < 3) {
//            normalAttack(target);
//        } else {
//            int attackChoice = attackChoiceMenu(getLevel());
//            if (attackChoice == 1) {
//                normalAttack(target);
//            } else {
//                specialAttack(attackChoice);
//            }
//        }
    }

    public int attackChoiceMenu(int level) {
        System.out.println("Attack options level: "+level);
        for (String text:attackStrings){
            System.out.println(text);
        }
        return Keyboard.readInt("Make a choice:");

    }

    public void normalAttack(Creature target) {
        int totalAttack = 0;
        int totalDefense = target.getTotalArmour();
        int attackDiceThrow = Dices.d20.getValue();

        if (weapon instanceof Sword) {
            totalAttack = attackDiceThrow + combatModifiers.getSwordAttackModifier();
        } else if (weapon instanceof Bow) {
            totalAttack = attackDiceThrow + combatModifiers.getBowAttackModifier();
        } else if (weapon instanceof HolySymbol) {
            totalAttack = attackDiceThrow + combatModifiers.getCurseAttackModifier();
        }

        int damage = 0;
        if (totalAttack <= totalDefense) {
            System.out.println("Your attack failed!");
        } else {
            if (weapon instanceof Sword) {
                damage = Dices.d10.getValue() + combatModifiers.getSwordDamageModifier();
            } else if (weapon instanceof Bow) {
                damage = Dices.d12.getValue() + combatModifiers.getBowDamageModifier();
            } else if (weapon instanceof HolySymbol) {
                damage = Dices.d6.getValue() + combatModifiers.getCurseDamageModifier();
            }
            System.out.println("You did " + damage + " damage!");
            target.setHitPoints(target.getHitPoints() - damage);
            if (target.getHitPoints() <= 0 ) {
                System.out.println("Your opponent has died!");
            }
        }
    }

    public void specialAttack (int attackCode) {
        System.out.println("Perform special attack of choice");
    }

    // Cheat
    public void bumpAllStats () {
        abilities.setSpeed(abilities.getSpeed()*2);
        abilities.setStrength(abilities.getStrength()*2);
        abilities.setConstitution(abilities.getConstitution()*2);
        abilities.setWisdom(abilities.getWisdom()*2);
        abilities.setCharisma(abilities.getCharisma()*2);
        abilities.setDexterity(abilities.getDexterity()*2);
        abilities.setIntelligence(abilities.getIntelligence()*2);
        abilities.setBaseArmour(abilities.getBaseArmour()*2);
        setHitPoints(getHitPoints()*2);
        applyAllCombatModifiers();
    }

    // Modifiers

    public void applyAllRaceModifiers () {
        abilities.setSpeed(abilities.getSpeed() + race.getBaseSpeed());
        abilities.setStrength(abilities.getStrength() + race.getStrength());
        abilities.setConstitution(abilities.getConstitution() + race.getConstitution());
        abilities.setWisdom(abilities.getWisdom() + race.getWisdom());
        abilities.setCharisma(abilities.getCharisma() + race.getCharisma());
        abilities.setDexterity(abilities.getDexterity() + race.getDexterity());
        abilities.setIntelligence(abilities.getIntelligence() + race.getIntelligence());
    }

    public void applyAllCombatModifiers () {
        calculateAndSetProficiency();
        calculateAndSetTotalArmour();
        applyStrengthModifier();
        applyConstitutionModifier();
        applyWisdomModifier();
        applyCharismaModifier();
        applyDexterityModifier();
        applyIntelligenceModifier();
        applySwordAttackModifier();
        applySwordDamageModifier();
        applyBowAttackModifier();
        applyBowDamageModifier();
        applyCurseAttackModifier();
        applyCurseDamageModifier();
    }

    public void calculateAndSetProficiency () {
        combatModifiers.setProficiency(Utilities.returnProf(getLevel()));
    }

    public void calculateAndSetTotalArmour () {
        combatModifiers.setTotalArmour(abilities.getBaseArmour() + armour.getArmourClass(abilities));
    }

    public void applyStrengthModifier () {
        combatModifiers.setStrengthModifier(Utilities.modify(getAbilities().getStrength()));
    }

    public void applyConstitutionModifier () {
        combatModifiers.setConstitutionModifier(Utilities.modify(getAbilities().getConstitution()));
    }

    public void applyWisdomModifier () {
        combatModifiers.setWisdomModifier(Utilities.modify(getAbilities().getWisdom()));
    }

    public void applyCharismaModifier () {
        combatModifiers.setCharismaModifier(Utilities.modify(getAbilities().getCharisma()));
    }

    public void applyDexterityModifier () {
        combatModifiers.setDexterityModifier(Utilities.modify(getAbilities().getDexterity()));
    }

    public void applyIntelligenceModifier () {
        combatModifiers.setIntelligenceModifier(Utilities.modify(getAbilities().getIntelligence()));
    }

    public void applySwordAttackModifier () {
        combatModifiers.setSwordAttackModifier(combatModifiers.getProficiency() + combatModifiers.getStrengthModifier());
    }

    public void applySwordDamageModifier () {
        combatModifiers.setSwordDamageModifier(combatModifiers.getStrengthModifier());
    }

    public void applyBowAttackModifier () {
        combatModifiers.setBowAttackModifier(combatModifiers.getProficiency() + combatModifiers.getDexterityModifier());
    }

    public void applyBowDamageModifier () {
        combatModifiers.setBowDamageModifier(combatModifiers.getDexterityModifier());
    }

    public void applyCurseAttackModifier() {
        combatModifiers.setCurseAttackModifier(combatModifiers.getProficiency() + combatModifiers.getWisdomModifier());
    }

    public void applyCurseDamageModifier() {
        combatModifiers.setCurseDamageModifier(combatModifiers.getWisdomModifier());
    }





//    Getters and Setters

    public Races getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    @Override
    public int getHitPoints() {
        return hitPoints;
    }
    @Override
    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public Abilities getAbilities() {
        return abilities;
    }

    public void setAbilities(Abilities abilities) {
        this.abilities = abilities;
    }
    @Override
    public int getSpeed () {
        return this.getAbilities().getSpeed();
    }

    public Pack getPack() {
        return pack;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public AdditionalArmour getShield() {
        return shield;
    }

    public void setShield(AdditionalArmour shield) {
        this.shield = shield;
    }

    public Armour getArmour() {
        return armour;
    }

    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    public int getGold() {
        return pack.getGoldPieces();
    }

    public void setGold(int gold) {
        pack.setGoldPieces(gold);
    }

    public CombatModifiers getCombatModifiers() {
        return combatModifiers;
    }

    public void setCombatModifiers(CombatModifiers combatModifiers) {
        this.combatModifiers = combatModifiers;
    }
    @Override
    public boolean isKey() {
        return key;
    }
    @Override
    public void setKey(boolean key) {
        this.key = key;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
        calculateLevel();
    }

    public void addXp(int addXp) {
        setXp(this.xp + addXp);
    }

    public void calculateLevel(){
        if (xp >= 3000) {
            setLevel(5);
        } else if (xp >= 1500) {
            setLevel(4);
        } else if (xp >= 700) {
            setLevel(3);
        } else if (xp >= 250) {
            setLevel(2);
        } else if (xp >= 100) {
            setLevel(1);
        }
    }

    public void addAttackString(String attackString){
        attackStrings.add(attackString);
    }

    @Override
    public int getTotalArmour () {
        return combatModifiers.getTotalArmour();
    }

    @Override
    public String toString() {
        return "PlayableCreature{" +
                "race=" + race +
                ", level=" + level +
                ", hitPoints=" + hitPoints +
                ", abilities=" + abilities +
                ", combatModifiers=" + combatModifiers +
                ", pack=" + pack +
                ", weapon=" + weapon +
                ", shield=" + shield +
                ", armour=" + armour +
                '}';
    }
}
