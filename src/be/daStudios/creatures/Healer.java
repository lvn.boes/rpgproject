package be.daStudios.creatures;

import be.daStudios.armour.*;
import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.backpack.ClericalBackpack;
import be.daStudios.backpack.Pack;
import be.daStudios.equippable.weapons.Dagger;
import be.daStudios.equippable.weapons.HolySymbol;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.utilities.Dices;

public class Healer extends PlayableCreature{

    public Healer (Race race) {
        setRace(race);
        setLevel(1);
        setHitPoints(Dices.d6.getValue());
        setAbilities(new Abilities (0,8,14,16,14,11,13, 4));
        setCombatModifiers(new CombatModifiers(0,0,0,0,0,0,0,0,
                0,0,0,0,0,0));
        setPack(new ClericalBackpack());
        getPack().addGold(15);
        setWeapon(new HolySymbol());
        getPack().addItem(new Dagger());
        setArmour(new Clothes());
        applyAllRaceModifiers();
        applyAllCombatModifiers();
        setKey(false);
    }

}
