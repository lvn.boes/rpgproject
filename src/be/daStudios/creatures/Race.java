package be.daStudios.creatures;

public enum Race implements Races{
    Human(6,1,1,1,1,1, 1),
    Dwarf(5,2,2,0,0,0,0),
    Elf(7,0,0, 2,0,2,0);

    private final int baseSpeed;
    private final int strength;
    private final int constitution;
    private final int dexterity;
    private final int wisdom;
    private final int intelligence;
    private final int charisma;

    Race(int baseSpeed, int strengthAbility, int constitution, int dexterityAbility, int wisdomAbility, int intelligenceAbility, int charismaAbility) {
        this.baseSpeed = baseSpeed;
        this.strength = strengthAbility;
        this.constitution = constitution;
        this.dexterity = dexterityAbility;
        this.wisdom = wisdomAbility;
        this.intelligence = intelligenceAbility;
        this.charisma = charismaAbility;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }

    public int getStrength() {
        return strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getWisdom() {
        return wisdom;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getCharisma() {
        return charisma;
    }
}
