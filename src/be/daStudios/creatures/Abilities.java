package be.daStudios.creatures;

import java.io.Serializable;

public class Abilities implements Serializable {
    private int speed;
    private int strength;
    private int constitution;
    private int wisdom;
    private int charisma;
    private int dexterity;
    private int intelligence;
    private int baseArmour;


    public Abilities(int speed, int strength, int constitution, int wisdom, int charisma, int dexterity, int intelligence, int baseArmour) {
        this.speed = speed;
        this.strength = strength;
        this.constitution = constitution;
        this.wisdom = wisdom;
        this.charisma = charisma;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.baseArmour = baseArmour;
    }


    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getBaseArmour() {
        return baseArmour;
    }

    public void setBaseArmour(int baseArmour) {
        this.baseArmour = baseArmour;
    }
}
