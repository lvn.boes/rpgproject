package be.daStudios.saveGame;

import be.daStudios.account.Account;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class FileSaveGame {
    private final static Path path = Path.of("memory").resolve("doc.txt");
    private static List<Account> accountList=new ArrayList<>();
    private boolean empty=true;

    public static void createFilePath(){
        try {
            Files.createDirectories(path.getParent());
            if (Files.notExists(path)) {
                Files.createFile(path);
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public static void saveGame(){

        try (FileOutputStream fos = new FileOutputStream(path.toString());
             ObjectOutputStream oOS = new ObjectOutputStream(fos);
        ) {

            for(Account account:accountList) {
                oOS.writeObject(account);
            }
            System.out.println();

        } catch (IOException ioException) {
            System.out.println("Couldn't save the game");
        }
    }


    public void loadSavedAccounts(){
        List<Account> newList=new ArrayList<>();

        try ( FileInputStream fis = new FileInputStream(path.toString());
              ObjectInputStream inReader = new ObjectInputStream(fis)
        ) {

            while (inReader.available()==0) {
                newList.add((Account) inReader.readObject());
            }


        } catch (IOException|ClassNotFoundException ioException) {

        }
        for (Account account:newList){
            if(!accountList.contains(account)){
                addAccount(account);
            }
        }
        System.out.println("Accounts are loaded");
    }
    public Account getAccount(int index){
        return accountList.get(index-1);
    }

    public void addAccount(Account account){
        accountList.add(account);
        empty=false;
        FileSaveGame.saveGame();
    }

    public void removeAccount(int index){
        accountList.remove(index);
        if(accountList.size()==0){
            empty=true;
        }
        FileSaveGame.saveGame();
    }

    public void resetAllAccounts(){
        accountList=new ArrayList<>();
        empty=true;
        FileSaveGame.saveGame();
    }
    public int getlistSize(){
        return accountList.size();
    }
    public boolean isEmpty(){
        return empty;
    }

    public String contentSavedAccounts(){
        StringBuilder sb=new StringBuilder("Saved accounts: \n");
        if(accountList.size()>0){
            for(int i=0;i<accountList.size();i++ ){
                sb.append(i+1).append(". ");
                sb.append(accountList.get(i).getLastSaved().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                sb.append(" User name: ").append(accountList.get(i).getUserName()).append("\n");
            }
        }else {
            sb.append("You haven't got saved accounts yet\n");
        }
        return sb.toString();
    }
}
