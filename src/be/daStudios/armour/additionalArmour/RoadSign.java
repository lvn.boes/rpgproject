package be.daStudios.armour.additionalArmour;

import be.daStudios.creatures.Abilities;

public class RoadSign implements AdditionalArmour {
    @Override
    public String getName() {
        return "Road sign";
    }

    @Override
    public String getDescription() {
        return "gives at least some cover";
    }

    @Override
    public int getArmourClass(Abilities abilities) {
        return 1;
    }
}
