package be.daStudios.armour.additionalArmour;

import be.daStudios.creatures.Abilities;

public class WoodShield implements AdditionalArmour{
    @Override
    public String getName() {
        return "Wood shield";
    }

    @Override
    public String getDescription() {
        return "a bit cracked but still useful";
    }

    @Override
    public int getArmourClass(Abilities abilities) {
        return 3;
    }
}
