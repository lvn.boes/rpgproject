package be.daStudios.armour.additionalArmour;

import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.creatures.Abilities;

public class Shield implements AdditionalArmour {

    @Override
    public String getName() {
        return "Shield";
    }

    @Override
    public String getDescription() {
        return "a reinforced shield, all you can wish for";
    }

    @Override
    public int getArmourClass(Abilities abilities) {
        return 5;
    }
}
