package be.daStudios.armour;

import be.daStudios.creatures.Abilities;

public class ChainMail implements Armour {
    private int armourBase=8;

    @Override
    public String getName() {
        return "Chain mail";
    }

    @Override
    public String getDescription() {
        return "Many small rings make one big strong jacket";
    }

    @Override
    public int getArmourClass(Abilities abilities) {
        return (int) ((double)this.armourBase+((double)armourBase*abilities.getStrength()/100));
    }

}
