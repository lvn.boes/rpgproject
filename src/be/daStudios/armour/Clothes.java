package be.daStudios.armour;

import be.daStudios.creatures.Abilities;

public class Clothes implements Armour {
    private final int armourBase=4;

    @Override
    public String getName() {
        return "Clothes";
    }

    @Override
    public String getDescription() {
        return "A linen or woollen tunic that will rip with the slightest touch";
    }

    @Override
    public int getArmourClass(Abilities abilities) {
        return(int)((double)this.armourBase+armourBase*((double)abilities.getDexterity()/100));
    }
}
