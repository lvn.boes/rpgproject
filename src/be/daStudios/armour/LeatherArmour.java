package be.daStudios.armour;

import be.daStudios.creatures.Abilities;
import be.daStudios.creatures.CombatModifiers;

public class LeatherArmour implements Armour {
    private final int armourBase=4;

    @Override
    public String getName() {
        return "Leather armour";
    }

    @Override
    public String getDescription() {
        return "A rough animal hide that is tanned and tailored to the user. Great for agile work";
    }

    @Override
    public int getArmourClass(Abilities abilities) {

        return (int)((double)this.armourBase+armourBase*(((double)(abilities.getStrength()+abilities.getDexterity())/2)/100));
    }
}
