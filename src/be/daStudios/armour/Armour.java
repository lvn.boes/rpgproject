package be.daStudios.armour;

import be.daStudios.creatures.Abilities;
import be.daStudios.equippable.Storable;

import java.io.Serializable;

public interface Armour extends Storable, Serializable {
    String getDescription();
    int getArmourClass(Abilities abilities);
}
