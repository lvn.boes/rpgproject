package be.daStudios.utilities;

import java.util.Random;

public class Utilities {
    private static Random random = new Random();

    public static int throwDice (int maxNumber) {
        return random.nextInt(maxNumber) + 1;
    }

    public static int randInt (int max) {
        return random.nextInt(max);
    }
    public static int randInt (int min, int max) {
        return min + random.nextInt(max-min);
    }


    public static int modify (int abilityParameter) {
        return (abilityParameter - 10)/2;
    }

    public static int returnProf (int level) {
        return level/2;
    }
}
