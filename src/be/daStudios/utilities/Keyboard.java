package be.daStudios.utilities;

import java.util.Scanner;

public class Keyboard {

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * When response is not valid, print question again.
     *
     * @param question
     * @return (user Input and define it into resp)
     */
    public static String readString(String question) {
        String resp = null;
        while (resp == null) {
            System.out.print(question);
            resp = scanner.nextLine();
        }
        return resp;
    }

    /**
     * Ask for a number.
     * @param question
     * @return the number
     */
    public static int readInt(String question) {
        return readInt(question, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Prints a prompt for user input and scans for an int. Chosen number has to be between min and max.
     *
     * @param question
     * @param min
     * @param max
     * @return a number between min and max
     */
    public static int readInt(String question, int min, int max) {
        int response = Integer.MIN_VALUE;
        do {
            System.out.print(question);
            String input = scanner.nextLine();
            try {
                response = Integer.parseInt(input);
            } catch(NumberFormatException nfe) {
                System.out.println("\"" + input + "\" is not a valid input for an integer!");
            }
        } while(response > max || response < min || response == Integer.MIN_VALUE);
        return response;
    }

    public static boolean validateYN(){
        while (true) {
            String choice= Keyboard.readString("y/n");
            if(choice.equals("y")){
                return true;
            }else if(choice.equals("n")){
                return false;
            }else {
                System.out.println("invalid input");
            }
        }
    }
}


