package be.daStudios.utilities;

import java.io.Serializable;
import java.util.Random;

public enum Dices implements Serializable {
    d2(2),
    d4(4),
    d6(6),
    d8(8),
    d10(10),
    d12(12),
    d20(20),
    d100(100);

    private int value;
    Dices(int value){
        this.value= value;

    }

    public int getValue() {
        Random random= new Random();
        return random.nextInt(value)+1;

    }
}
