package be.daStudios.equippable.weapons;

public class Club implements Weapon{
    @Override
    public String getAttackString() {
        return "swings at";
    }

    @Override
    public String getDescription() {
        return "wood can be harmful";
    }

    @Override
    public String getName() {
        return "club";
    }
}
