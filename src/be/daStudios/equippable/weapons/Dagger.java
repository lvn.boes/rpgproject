package be.daStudios.equippable.weapons;

public class Dagger implements Weapon{
    @Override
    public String getAttackString() {
        return "stings";
    }

    @Override
    public String getDescription() {
        return "small but deadly";
    }

    @Override
    public String getName() {
        return "dagger";
    }
}
