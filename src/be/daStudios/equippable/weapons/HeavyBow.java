package be.daStudios.equippable.weapons;

public class HeavyBow implements Weapon{
    @Override
    public String getAttackString() {
        return "shoots an heavy arrow at";
    }

    @Override
    public String getDescription() {
        return "a bow but more deadly";
    }

    @Override
    public String getName() {
        return "heavy bow";
    }
}
