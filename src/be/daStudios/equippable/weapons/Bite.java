package be.daStudios.equippable.weapons;

import be.daStudios.equippable.Storable;

public class Bite implements Weapon {
    private final String name="bite";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAttackString() {
        return "bites you!!";
    }

    @Override
    public String getDescription() {
        return "A bite seems ok but is really painful";
    }

}
