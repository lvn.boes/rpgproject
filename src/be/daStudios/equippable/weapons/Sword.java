package be.daStudios.equippable.weapons;

public class Sword implements Weapon{
    @Override
    public String getAttackString() {
        return "swings at";
    }

    @Override
    public String getDescription() {
        return "it is what it says";
    }

    @Override
    public String getName() {
        return "sword";
    }
}
