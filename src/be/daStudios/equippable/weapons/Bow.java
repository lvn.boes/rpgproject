package be.daStudios.equippable.weapons;

import be.daStudios.equippable.Storable;

public class Bow implements Weapon {
    @Override
    public String getName() {
        return "bow";
    }

    @Override
    public String getAttackString() {
        return "shoots an arrow at";
    }

    @Override
    public String getDescription() {
        return "great for long range fights";
    }
}
