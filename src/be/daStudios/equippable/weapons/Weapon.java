package be.daStudios.equippable.weapons;

import be.daStudios.equippable.Storable;

public interface Weapon extends Storable {
    String getAttackString();
    String getDescription();
}
