package be.daStudios.equippable.weapons;

public class HolySymbol implements Weapon{
    @Override
    public String getAttackString() {
        return "puts a spell on";
    }

    @Override
    public String getDescription() {
        return "just magic";
    }

    @Override
    public String getName() {
        return "holy symbol";
    }
}
