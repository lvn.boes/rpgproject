package be.daStudios.equippable;


import java.io.Serializable;

public interface Storable extends Serializable {
    String getName();
}
