package be.daStudios.account;

import be.daStudios.map.CoordinatePair;
import be.daStudios.map.PassableTerrain;
import be.daStudios.map.Terrain;
import be.daStudios.map.TerrainType;

public class PlayGame {
    private Game game;
    private Terrain terrain;

    public PlayGame(Game game){
        this.game=game;
        setTerrain();
    }

    private void setTerrain(){
        terrain= game.getMap().getMapArray()[game.getYpos()][game.getXpos()];
    }

    public boolean searchCreature(){
        return terrain.isPassed();
    }

    public boolean moveForward(){
        int move= game.getYpos()-1;
        if (move>=0) {
            if (game.getMap().getMapArray()[move][game.getXpos()] instanceof PassableTerrain &&
                    ((PassableTerrain) game.getMap().getMapArray()[move][game.getXpos()]).isLockedGate()) {
                if (game.getPlayer().isKey()) {
                    ((PassableTerrain) game.getMap().getMapArray()[move][game.getXpos()]).setLockedGate(false);
                    game.getMap().getMapArray()[move][game.getXpos()].setVisual(TerrainType.PATH.getVisual());
                    game.setYpos(move);
                    setTerrain();
                    game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                    System.out.println("You have unlocked the gate!");
                    return true;
                } else {
                    System.out.println("This gate is locked");
                    return false;
                }
            } else if(game.getMap().getMapArray()[move][game.getXpos()] instanceof PassableTerrain) {
                game.setYpos(move);
                setTerrain();
                game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                return true;
            }else {
                System.out.println("Non passable terrain");
                return false;
            }
        }else {
            System.out.println("End of the map");
            return false;
        }
    }


    public boolean moveBack(){
        int move= game.getYpos()+1;
        if (move<game.getMap().getMapArray().length) {
            if (game.getMap().getMapArray()[move][game.getXpos()] instanceof PassableTerrain &&
                    ((PassableTerrain) game.getMap().getMapArray()[move][game.getXpos()]).isLockedGate()) {
                if (game.getPlayer().isKey()) {
                    ((PassableTerrain) game.getMap().getMapArray()[move][game.getXpos()]).setLockedGate(false);
                    game.getMap().getMapArray()[move][game.getXpos()].setVisual(TerrainType.PATH.getVisual());
                    game.setYpos(move);
                    setTerrain();
                    game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                    System.out.println("You have unlocked the gate!");
                    return true;
                } else {
                    System.out.println("This gate is locked");
                    return false;
                }
            } else if(game.getMap().getMapArray()[move][game.getXpos()]  instanceof PassableTerrain) {
                game.setYpos(move);
                setTerrain();
                game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                return true;
            }else {
                System.out.println("Non passable terrain");
                return false;
            }
        }else {
            System.out.println("End of the map");
            return false;
        }
    }

    public boolean moveLeft(){
        int move= game.getXpos()-1;
        if (move>=0) {
            if (game.getMap().getMapArray()[game.getYpos()][move]  instanceof PassableTerrain &&
                    ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][move]).isLockedGate()) {
                if (game.getPlayer().isKey()) {
                    ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][move]).setLockedGate(false);
                    game.getMap().getMapArray()[game.getYpos()][move].setVisual(TerrainType.PATH.getVisual());
                    game.setXpos(move);
                    setTerrain();
                    game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                    System.out.println("You have unlocked the gate!");
                    return true;
                } else {
                    System.out.println("This gate is locked");
                    return false;
                }
            } else if(game.getMap().getMapArray()[game.getYpos()][move]  instanceof PassableTerrain) {
                game.setXpos(move);
                setTerrain();
                game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                return true;
            }else {
                System.out.println("Non passable terrain");
                return false;
            }
        }else {
            System.out.println("End of the map");
            return false;
        }
    }

    public boolean moveRight(){
        int move= game.getXpos()+1;
        if (move<game.getMap().getMapArray()[0].length) {
            if (game.getMap().getMapArray()[game.getYpos()][move]  instanceof PassableTerrain &&
                    ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][move]).isLockedGate()) {
                if (game.getPlayer().isKey()) {
                    ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][move]).setLockedGate(false);
                    game.getMap().getMapArray()[game.getYpos()][move].setVisual(TerrainType.PATH.getVisual());
                    game.setXpos(move);
                    setTerrain();
                    game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                    System.out.println("You have unlocked the gate!");
                    return true;
                } else {
                    System.out.println("This gate is locked");
                    return false;
                }
            } else if(game.getMap().getMapArray()[game.getYpos()][move]  instanceof PassableTerrain) {
                game.setXpos(move);
                setTerrain();
                game.getPassedCoordinates().add(new CoordinatePair(game.getXpos(), game.getYpos()));
                return true;
            }else {
                System.out.println("Non passable terrain");
                return false;
            }
        }else {
            System.out.println("End of the map");
            return false;
        }
    }
    public Terrain getTerrain(){
        return terrain;
    }
}
