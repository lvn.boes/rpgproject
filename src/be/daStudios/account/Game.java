package be.daStudios.account;

import be.daStudios.creatures.Creature;
import be.daStudios.map.CoordinatePair;
import be.daStudios.map.Map;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;

public class Game implements Serializable {
    private final String name;
    private Creature player;
    private Map map;
    private int startXpos;
    private int startYpos;
    private int currentXpos;
    private int currentYpos;
    private LocalDateTime lastSaved;
    private HashSet<CoordinatePair> passedCoordinates= new HashSet<>();


    public Game(Creature player,Map map,String name){
        this.name=name;
        this.player=player;
        this.map=map;
        this.startXpos=map.getStartPosition().getX();
        this.startYpos=map.getStartPosition().getY();
        this.currentXpos=map.getStartPosition().getX();
        this.currentYpos=map.getStartPosition().getY();
        this.passedCoordinates.add(map.getStartPosition());
    }

    public Creature getPlayer() {
        return player;
    }

    public void setPlayer(Creature player) {
        this.player = player;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public CoordinatePair getCurrentPosition() {
        return CoordinatePair.set(currentXpos,currentYpos);
    }

    public void setCurrentPosition(CoordinatePair currentPosition) {
        this.currentXpos=currentPosition.getX();
        this.currentYpos=currentPosition.getY();
    }

    public LocalDateTime getLastSaved() {
        return lastSaved;
    }

    public void setLastSaved(LocalDateTime lastSaved) {
        this.lastSaved = lastSaved;
    }

    public int getStartXpos() {
        return startXpos;
    }

    public int getStartYpos() {
        return startYpos;
    }

    public int getXpos() {
        return currentXpos;
    }

    public void setXpos(int currentXpos) {
        this.currentXpos = currentXpos;
    }

    public int getYpos() {
        return currentYpos;
    }

    public void setYpos(int currentYpos) {
        this.currentYpos = currentYpos;
    }

    public String getName() {
        return name;
    }

    public HashSet<CoordinatePair> getPassedCoordinates() {
        return passedCoordinates;
    }

    public void setPassedCoordinates(HashSet<CoordinatePair> passedCoordinates) {
        this.passedCoordinates = passedCoordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return currentXpos == game.currentXpos &&
                currentYpos == game.currentYpos &&
                Objects.equals(name, game.name) &&
                Objects.equals(player, game.player) &&
                Objects.equals(map, game.map) &&
                Objects.equals(lastSaved, game.lastSaved);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, player, map, currentXpos, currentYpos, lastSaved);
    }
}
