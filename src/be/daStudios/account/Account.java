package be.daStudios.account;

import be.daStudios.saveGame.FileSaveGame;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Account implements Serializable {
    private LocalDateTime lastSaved;
    private String userName;
    private String userPassword;
    private List<Game> savedGames=new ArrayList<>();

    public Account(String userName,String userPassword){
        this.userName=userName;
        this.userPassword=userPassword;
        lastSaved=LocalDateTime.now();
    }

    public LocalDateTime getLastSaved() {
        return lastSaved;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        FileSaveGame.saveGame();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        FileSaveGame.saveGame();
    }
    public void addGame(Game newGame){
        savedGames.add(newGame);
        FileSaveGame.saveGame();
    }

    public void removeGame(int index){
        savedGames.remove(index);
        FileSaveGame.saveGame();
    }
    public Game getGame(int index){
        return savedGames.get(index-1);
    }
    public int getListSize(){
        return savedGames.size();
    }

    public String contentSavedGames(){
        StringBuilder sb=new StringBuilder("Saved games: \n");
        if(savedGames.size()>0){
            for(int i=0;i<savedGames.size();i++ ){
                sb.append(i+1).append(". ");
                sb.append(savedGames.get(i).getName());
//                sb.append(savedGames.get(i).getLastSaved().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            }
        }else {
            sb.append("You haven't got saved games yet");
        }
        return sb.toString();
    }
    public String getSecretPassword(){
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<userPassword.length();i++){
            sb.append("*");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(lastSaved, account.lastSaved) &&
                Objects.equals(userName, account.userName) &&
                Objects.equals(userPassword, account.userPassword) &&
                Objects.equals(savedGames, account.savedGames);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastSaved, userName, userPassword, savedGames);
    }

    @Override
    public String toString() {
        return "Account{" +
                "lastSaved=" + lastSaved +
                ", userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                contentSavedGames()+
                '}';
    }
}
