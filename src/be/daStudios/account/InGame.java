package be.daStudios.account;

import be.daStudios.armour.Armour;
import be.daStudios.armour.additionalArmour.AdditionalArmour;
import be.daStudios.creatures.Creature;
import be.daStudios.creatures.Monster;
import be.daStudios.creatures.PlayableCreature;
import be.daStudios.equippable.Storable;
import be.daStudios.equippable.weapons.Weapon;
import be.daStudios.map.CoordinatePair;
import be.daStudios.map.Map;
import be.daStudios.map.PassableTerrain;
import be.daStudios.menu.TextMenus;
import be.daStudios.utilities.Keyboard;

public class InGame {
    private final PlayableCreature player;
    private Monster monster;
    private Game game;
    private PlayGame playGame;
    private boolean yourTurn = true;
    private int totalActionsPerTurn;
    private int actionsLeft;

    public InGame(Game game) {
        this.game = game;
        this.playGame = new PlayGame(game);
        player=(PlayableCreature) game.getPlayer();
    }


    public void runGame() {
        boolean input = true;
        totalActionsPerTurn = game.getPlayer().getSpeed();
        System.out.printf("Welcome to %s %n", game.getMap().getName());
        while (input) {
            actionsLeft = totalActionsPerTurn;
            System.out.println("Your turn has started!");
            while (yourTurn&&actionsLeft>0) {
                System.out.println("HP: "+player.getHitPoints()+" XP: "+player.getXp()+" Level: "+player.getLevel());
                System.out.println("Actions left in this turn: " + actionsLeft);
                Map.generateAndPrintMinimapFromPosition(game.getMap(), game.getXpos(), game.getYpos());
                String inputString=Keyboard.readString("choice: ");

                switch (inputString) {
                    case "Fight":
                        if(monster!=null) {
                            fightWithMonster();
                        }else{
                            System.out.println("There is no monster here");
                        }
                        break;
                    case "z":
                    case "w":
                        if (playGame.moveForward()) {
                            setMonster();
                            actionsLeft--;
                            if (!playGame.getTerrain().isPassed() && playGame.getTerrain() instanceof PassableTerrain) {
                                System.err.println("There is a " + monster.getRace() + " here with " + monster.getHitPoints() + " hitpoints!");
                            }
                        }
                        break;
                    case "s":
                        if (playGame.moveBack()) {
                            setMonster();
                            actionsLeft--;
                            if (!playGame.getTerrain().isPassed() && playGame.getTerrain() instanceof PassableTerrain) {
                                System.err.println("There is a " + monster.getRace() + " here with " + monster.getHitPoints() + " hitpoints!");
                            }
                        }
                        break;
                    case "q":
                    case "a":
                        if (playGame.moveLeft()) {
                            setMonster();
                            actionsLeft--;
                            if (!playGame.getTerrain().isPassed() && playGame.getTerrain() instanceof PassableTerrain) {
                                System.err.println("There is a " + monster.getRace() + " here with " + monster.getHitPoints() + " hitpoints!");
                            }
                        }
                        break;
                    case "d":
                        if (playGame.moveRight()) {
                            setMonster();
                            actionsLeft--;
                            if (!playGame.getTerrain().isPassed() && playGame.getTerrain() instanceof PassableTerrain) {
                                System.err.println("There is a " + monster.getRace() + " here with " + monster.getHitPoints() + " hitpoints!");
                            }
                        }
                        break;
                    case "Backpack":
                        switchItem();
                        break;
                    case "BackpackContent":
                        System.out.println(player.getPack().getPackContent());
                        System.out.println("Return?");
                        Keyboard.validateYN();
                        break;
                    case "Legend":
                        Map.printMapLegend();
                        break;
                    case "Map":
                        Map.printExploredMap(game.getMap(), game.getPassedCoordinates(), new CoordinatePair(game.getXpos(), game.getYpos()));
                        break;
                    case "MarcoPolo":
                        game.getMap().printMapColours();
                        System.out.println("Here is the complete map!\nEnjoy it, you filthy cheater!!!");
                        break;
                    case "TheMightyHulk":
                        if (game.getPlayer() instanceof PlayableCreature) {
                            ((PlayableCreature) game.getPlayer()).bumpAllStats();
                            System.out.println("All your stats have been increased twofold, oh mighty Hulk!!!");
                        }
                        break;
                    case "Loot":
                        loot();
                        actionsLeft--;
                        break;
                    case "Controls":
                        System.out.println(TextMenus.controls());
                        break;
                    case "Quit":
                        yourTurn = false;
                        input = false;
                        break;
                    default:
                        System.out.println("invalid input");
                }
                if (game.getPlayer().isKey() && game.getStartXpos()==game.getXpos() && game.getStartYpos() == game.getYpos()) {
                    System.out.println("Victory!!!");
                    input = false;
                }
            }
            creaturesTurn(input);
            yourTurn = true;
            if (game.getPlayer().getHitPoints() <= 0) {
                System.err.println("you died");
                input = false;
            }
        }
        System.out.println("Your game has been saved");
    }

    private void fightWithMonster() {
        game.getPlayer().attack(monster);
        int hitpointsAfterAttack = monster.getHitPoints();
        if (hitpointsAfterAttack <= 0) {
            checkIfDeadAndUpdateXpAndKey(monster, hitpointsAfterAttack);
            ((PassableTerrain) playGame.getTerrain()).setPassed(true);
        }
        actionsLeft -= 1;
    }

    private void loot(){
        if(monster!=null) {
            if (playGame.getTerrain().isPassed()) {
                StringBuilder sb = new StringBuilder("Loot found:\n");
                if (monster.getArmour() != null) {
                    player.getPack().addItem(monster.getArmour());
                    sb.append(monster.getArmour().getName()).append("\n");
                }
                if (monster.getWeapon() != null) {
                    player.getPack().addItem(monster.getWeapon());
                    sb.append(monster.getWeapon().getName()).append("\n");
                }
                if (monster.getSecondWeapon() != null) {
                    player.getPack().addItem(monster.getSecondWeapon());
                    sb.append(monster.getSecondWeapon().getName());

                }
                System.out.println(sb.toString());
            }else {
                System.out.println("Defeat the monster first");
            }
        }else {
            System.out.println("nothing to loot here");
        }
    }
    private void switchItem(){
        int inputInt;
        System.out.println(player.getPack().getPackContent());
        System.out.println("0. to exit");
        while (true) {
            inputInt = Keyboard.readInt("Choose an item to take from the backPack: ", 0, player.getPack().size());
            if (inputInt == 0) {
                break;
            } else if (inputInt > 0) {
                Storable item = player.getPack().getItem(inputInt-1);
                if (item instanceof AdditionalArmour) {
                    player.setShield((AdditionalArmour) player.getPack().switchItem(player.getShield(), inputInt-1));
                } else if (item instanceof Armour) {
                    player.setArmour((Armour) player.getPack().switchItem(player.getArmour(), inputInt-1));
                } else if (item instanceof Weapon) {
                    player.setWeapon((Weapon) player.getPack().switchItem(player.getWeapon(), inputInt-1));
                }
            } else {
                System.out.println("invalid input");
            }
        }
    }
    private void setMonster(){
        monster=(Monster) ((PassableTerrain)playGame.getTerrain()).getCreature();
    }

    private void checkIfDeadAndUpdateXpAndKey(Creature creature, int hitpointsAfterAttack) {
        if (hitpointsAfterAttack <= 0) {
            System.out.println("You killed the " + creature.getRace() + "!");
            ((PassableTerrain) playGame.getTerrain()).setPassed(true);
            if (creature instanceof Monster) {
                int xp = ((Monster) creature).getXp();
                if (game.getPlayer() instanceof PlayableCreature) {
                    ((PlayableCreature) game.getPlayer()).addXp(xp);
                }
                System.out.println("You gained " + xp + " xp!");
            }
            if (creature.isKey()) {
                creature.setKey(false);
                game.getPlayer().setKey(true);
                System.out.println("You have found the key! Now go back to your starting point to win!");
            }
        }
    }


    private void creaturesTurn(Boolean input) {
        if (input) {
            System.out.println("Your enemies turn!");
        }
        if (!game.getMap().getMapArray()[game.getYpos()][game.getXpos()].isPassed() &&
                game.getMap().getMapArray()[game.getYpos()][game.getXpos()] instanceof PassableTerrain) {
            int creatureActions = ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][game.getXpos()]).getCreature().getSpeed();
            boolean creatureTurn = input;
            while (creatureTurn) {
                ((PassableTerrain) game.getMap().getMapArray()[game.getYpos()][game.getXpos()]).getCreature().attack(game.getPlayer());
                if (game.getPlayer().getHitPoints() <= 0) {
                    System.out.println("You have been killed!\n Game over!");
                    creatureTurn = false;
                }
                creatureActions -= 1;
                if (creatureActions == 0) {
                    creatureTurn = false;
                }
            }
        }
    }
}

