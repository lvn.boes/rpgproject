package be.daStudios.map;

public enum TerrainType {
    FORREST('&'),
    WATER('~'),
    MARSH('_'),
    MOUNTAIN ('M'),
    CAVEWALL ('/'),
    MY_POSITION('o'),
    INVISIBLE_TERRAIN(' '),
    LIVING_CREATURE('x'),
    DEAD_CREATURE('✝'),
    LOCKED_GATE('.'),
    PATH('X');

    char visual;

    TerrainType(char visual) {
        this.visual = visual;
    }

    public char getVisual() {
        return visual;
    }

    public void setVisual(char visual) {
        this.visual = visual;
    }

}
