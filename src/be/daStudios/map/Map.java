package be.daStudios.map;

import be.daStudios.creatures.Creature;
import be.daStudios.creatures.MonsterRace;
import be.daStudios.creatures.Monster;
import be.daStudios.utilities.Dices;
import be.daStudios.utilities.Utilities;

import java.io.Serializable;
import java.util.HashSet;

public class Map implements Serializable {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private String name;
    private Terrain[][] mapArray;
    private int width;
    private int height;
    private CoordinatePair startPosition;

    public Map () {
    }


    public void initiateEmptyMap (int width, int height) {
        setWidth(width);
        setHeight(height);
        Terrain[][] map = new Terrain[height][];
        for (int i = 0 ; i < height ; i++) {
            map[i] = new Terrain[width];
            for (int j = 0 ; j < width ; j++) {
                map[i][j] = new NonPassableTerrain();
            }
        }
        setMapArray(map);
    }

    public void initiateForestOfStreams() {
        name="Forest of Streams";
        initiateEmptyMap(40, 40);
        startPosition = new CoordinatePair(6,39);
        createPath(6,0,6,2);
        createPath(7,2,8,2);
        createPath(8,3,8,5);
        createPath(7,5,1,5);
        createPath(1,6,1,17);
        createPath(2,17,11,17);
        createPath(3,18,3,36);
        createPath(4,36,34,36);
        createPath(34, 35, 34, 8);
        createPath(33,8,28,8);
        createPath(28,7,28,3);
        createPath(11,16, 11, 9);
        createPath(12,9,18,9);
        createPath(18,10,18,13);
        createPath(19,13,23,13);
        createPath(23,12,23,4);
        createPath(22,4,18,4);
        createPath(18,3,18,2);
        createPath(19,2,28,2);
        createPath(32,7,32,3);
        createPath(31,3,30,3);
        createPath(30,2,30,1);
        createPath(31,1,38,1);
        createPath(38,2,38,6);
        createPath(37,6,33,6);
        createPath(12,15,16,15);
        createPath(16,16,16,19);
        createPath(17,19,28,19);
        createPath(18,35,18,25);
        createPath(4,26,17,26);
        createPath(19,25,26,25);
        randomiseNonPassableTerrainTypes();
        setPathAsLockedGate(14,3);
        setPathAsLockedGate(12,3);
        if (mapArray[14][26] instanceof PassableTerrain) {
            ((PassableTerrain) mapArray[14][26]).setCreature(new Monster(MonsterRace.Bugbear));
        }
        if (mapArray[20][28] instanceof PassableTerrain) {
            ((PassableTerrain) mapArray[20][28]).setCreature(new Monster(MonsterRace.Wolf));
        }

    }

    public void initiateCaveOfThreads() {
        name="Cave of Threads";
        initiateEmptyMap(30, 30);
        startPosition = new CoordinatePair(12,29);
        addCornerPathInside(12,0, 12,4);
        addCornerPathOutside(12,4, 6, 3);
        addCornerPathOutside(12,4, 25, 7);

        addCornerPathOutside(12,4, 17, 13);
        addCornerPathOutside(17, 13, 3, 16);
        addCornerPathOutside(17, 13, 23, 18);

        addCornerPathOutside(17,13, 14, 22);
        addCornerPathInside(14, 22, 3, 27);
        addCornerPathOutside(14, 22, 24, 25);

        addCornerPathOutside(6, 3, 3, 16);
        addCornerPathInside(23, 18, 24, 25);

        setNonPassableTerrainToCaveWall();

        if (mapArray[2][3] instanceof PassableTerrain) {
            ((PassableTerrain) mapArray[2][3]).setCreature(new Monster(MonsterRace.Bugbear));
        }
        if (mapArray[20][28] instanceof PassableTerrain) {
            ((PassableTerrain) mapArray[22][25]).setCreature(new Monster(MonsterRace.Wolf));
        }
    }

    public void initiateFullRandomMap() {
        int mapChoice = Dices.d2.getValue();
        switch (mapChoice) {
            case 1:
                initiateRandomAboveGroundMap();
                break;
            case 2:
                initiateRandomUndergroundMap();
                break;
        }
    }

    public void initiateRandomAboveGroundMap() {
        name="Random map";
        initiateEmptyMap(40,40);
        generateRandomLoopPaths();
        randomiseNonPassableTerrainTypes();
    }

    public void initiateRandomUndergroundMap () {
        name="Random underground map";
        initiateEmptyMap(30,30);
        generateRandomTreePaths();
        setNonPassableTerrainToCaveWall();
    }

    public void createPath(int x_start, int y_start, int x_end, int y_end) {
        if (x_start == x_end) {
            addVerticalPath(x_start, (mapArray.length-1)-y_start, (mapArray.length-1)-y_end);
        }
        else if (y_start == y_end) {
            addHorizontalPath((mapArray.length-1)-y_start, x_start, x_end);
        }
        else {
            System.out.println("Invalid coördinates: you can only draw horizontal and vertical paths");
        }
    }

    public void addCornerPathOutside(int x_start, int y_start, int x_end, int y_end) {
        boolean x_start_low = x_start <= x_end;
        boolean y_start_low = y_start <= y_end;
        boolean startLeft = x_start <= mapArray[0].length/2;
        boolean startTop = y_start <= mapArray.length/2;
        if (startLeft && startTop && x_start_low && y_start_low) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (startLeft && startTop) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (!startLeft && startTop && x_start_low && !y_start_low) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (!startLeft && startTop) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (!startLeft && !startTop && !x_start_low && !y_start_low) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (!startLeft && !startTop) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (startLeft && !startTop && !x_start_low && y_start_low) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        }
    }
    public void addCornerPathInside(int x_start, int y_start, int x_end, int y_end) {
        boolean x_start_low = x_start <= x_end;
        boolean y_start_low = y_start <= y_end;
        boolean startLeft = x_start <= mapArray[0].length/2;
        boolean startTop = y_start <= mapArray.length/2;
        if (startLeft && startTop && x_start_low && y_start_low) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (startLeft && startTop) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (!startLeft && startTop && x_start_low && !y_start_low) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (!startLeft && startTop) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (!startLeft && !startTop && !x_start_low && !y_start_low) {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        } else if (!startLeft && !startTop) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else if (startLeft && !startTop && !x_start_low && y_start_low) {
            cornerYbeforeX(x_start, y_start, x_end, y_end);
        } else {
            cornerXbeforeY(x_start, y_start, x_end, y_end);
        }
    }
    public void cornerXbeforeY (int x_start, int y_start, int x_end, int y_end) {
        createPath(x_start, y_start, x_end, y_start);
        createPath(x_end, y_start, x_end, y_end);
    }
    public void cornerYbeforeX (int x_start, int y_start, int x_end, int y_end) {
        createPath(x_start, y_start, x_start,y_end);
        createPath(x_start, y_end, x_end, y_end);
    }

    public void addVerticalPath(int x, int y_start, int y_end) {
        int low = Math.min(y_start, y_end);
        int high = Math.max(y_start, y_end);
        for (int i = 0 ; i <= high - low ; i++) {
            PassableTerrain terrain = new PassableTerrain();
            int MonsterOrNot = Dices.d10.getValue();
            if (MonsterOrNot == 10) {
                terrain.setCreature(Monster.generateAndReturnRandomGoblin());
                terrain.setPassed(false);
            }
            mapArray[low + i][x] = terrain;
        }
    }
    public void addHorizontalPath (int y, int x_start, int x_end) {
        int low = Math.min(x_start, x_end);
        int high = Math.max(x_start, x_end);
        for (int i = 0 ; i <= high - low ; i++) {
            PassableTerrain terrain = new PassableTerrain();
            int MonsterOrNot = Dices.d10.getValue();
            if (MonsterOrNot == 10) {
                terrain.setCreature(Monster.generateAndReturnRandomGoblin());
                terrain.setPassed(false);
            }
            mapArray[y][low + i] = terrain;
        }
    }

    public void setPathAsLockedGate (int x, int y) {
        if (mapArray[y][x] instanceof PassableTerrain) {
            PassableTerrain lockedGate = new PassableTerrain();
            lockedGate.setLockedGate(true);
            lockedGate.setVisual(TerrainType.LOCKED_GATE.getVisual());
            mapArray[x][y] = lockedGate;
        }
    }

    public void setPathAsLockedGate (CoordinatePair coordinatePair) {
        int x = coordinatePair.getX();
        int y = coordinatePair.getY();
        if (mapArray[y][x] instanceof PassableTerrain) {
            PassableTerrain lockedGate = new PassableTerrain();
            lockedGate.setLockedGate(true);
            lockedGate.setVisual(TerrainType.LOCKED_GATE.getVisual());
            mapArray[x][y] = lockedGate;
        }
    }


    public void generateRandomLoopPaths() {
        int ht = mapArray.length/3;
        int wt = mapArray[0].length/3;
        int hq = mapArray.length/4;
        int wq = mapArray[0].length/4;
        CoordinatePair outside_1 = CoordinatePair.randomCoordinatePair(1,wq,1,hq);
        CoordinatePair outside_2 = CoordinatePair.randomCoordinatePair(1,wt,hq,2*hq);
        CoordinatePair outside_3 = CoordinatePair.randomCoordinatePair(1,wt,2*hq,3*hq);
        CoordinatePair outside_4 = CoordinatePair.randomCoordinatePair(1,wq,3*hq,4*hq-1);
        CoordinatePair outside_5 = CoordinatePair.randomCoordinatePair(wq,2*wq,4*hq-ht,4*hq-1);
        CoordinatePair outside_6 = CoordinatePair.randomCoordinatePair(2*wq,3*wq,4*hq-ht,4*hq-1);
        CoordinatePair outside_7 = CoordinatePair.randomCoordinatePair(3*wq,4*wq-1,3*hq,4*hq-1);
        CoordinatePair outside_8 = CoordinatePair.randomCoordinatePair(4*wq-wt,4*wq-1,2*hq,3*hq);
        CoordinatePair outside_9 = CoordinatePair.randomCoordinatePair(4*wq-wt,4*wq-1,hq,2*hq);
        CoordinatePair outside_10 = CoordinatePair.randomCoordinatePair(3*wq,4*wq-1,1,hq);
        CoordinatePair outside_11 = CoordinatePair.randomCoordinatePair(2*wq,3*wq,1,ht);
        CoordinatePair outside_12 = CoordinatePair.randomCoordinatePair(wq,2*wq,1,ht);

        CoordinatePair inside_1 = CoordinatePair.randomCoordinatePair(wq, 2*wq,hq,2*hq);
        CoordinatePair inside_2 = CoordinatePair.randomCoordinatePair(wq, 2*wq,2*hq,3*hq);
        CoordinatePair inside_3 = CoordinatePair.randomCoordinatePair(2*wq, 3*wq,2*hq,3*hq);
        CoordinatePair inside_4 = CoordinatePair.randomCoordinatePair(2*wq, 3*wq,hq,2*hq);

        startPosition = new CoordinatePair(outside_1.getX(), mapArray.length-1);
        addVerticalPath(outside_1.getX(), startPosition.getY(), outside_1.getY());
        addCornerPathOutside(outside_1.getX(), outside_1.getY(), outside_2.getX(), outside_2.getY());
        addCornerPathOutside(outside_2.getX(), outside_2.getY(), outside_3.getX(), outside_3.getY());
        addCornerPathOutside(outside_3.getX(), outside_3.getY(), outside_4.getX(), outside_4.getY());
        addCornerPathOutside(outside_4.getX(), outside_4.getY(), outside_5.getX(), outside_5.getY());
        addCornerPathOutside(outside_5.getX(), outside_5.getY(), outside_6.getX(), outside_6.getY());
        addCornerPathOutside(outside_6.getX(), outside_6.getY(), outside_7.getX(), outside_7.getY());
        addCornerPathOutside(outside_7.getX(), outside_7.getY(), outside_8.getX(), outside_8.getY());
        addCornerPathOutside(outside_8.getX(), outside_8.getY(), outside_9.getX(), outside_9.getY());
        addCornerPathOutside(outside_9.getX(), outside_9.getY(), outside_10.getX(), outside_10.getY());
        addCornerPathOutside(outside_10.getX(), outside_10.getY(), outside_11.getX(), outside_11.getY());
        addCornerPathOutside(outside_11.getX(), outside_11.getY(), outside_12.getX(), outside_12.getY());
        addCornerPathOutside(outside_12.getX(), outside_12.getY(), outside_1.getX(), outside_1.getY());

        CoordinatePair[] outRand_1 = {outside_12, outside_2};
        CoordinatePair outRandCoord1 = outRand_1[Utilities.randInt(2)];
        CoordinatePair[] outRand_2 = {outside_3, outside_5};
        CoordinatePair outRandCoord2 = outRand_2[Utilities.randInt(2)];
        CoordinatePair[] outRand_3 = {outside_6, outside_8};
        CoordinatePair outRandCoord3 = outRand_3[Utilities.randInt(2)];
        CoordinatePair[] outRand_4 = {outside_9, outside_11};
        CoordinatePair outRandCoord4 = outRand_4[Utilities.randInt(2)];

        addCornerPathInside(outRandCoord1.getX(), outRandCoord1.getY(), inside_1.getX(), inside_1.getY());
        addCornerPathInside(outRandCoord2.getX(), outRandCoord2.getY(), inside_2.getX(), inside_2.getY());
        addCornerPathInside(outRandCoord3.getX(), outRandCoord3.getY(), inside_3.getX(), inside_3.getY());
        addCornerPathInside(outRandCoord4.getX(), outRandCoord4.getY(), inside_4.getX(), inside_4.getY());

        CoordinatePair[] insideCoordinates = {inside_1, inside_2, inside_3, inside_4};
        int random1 = Utilities.randInt(4);
        int random2 = Utilities.randInt(4);
        while (random2 == random1) {
            random2 = Utilities.randInt(4);
        }
        addCornerPathInside(insideCoordinates[random1].getX(), insideCoordinates[random1].getY(),
                insideCoordinates[random2].getX(), insideCoordinates[random2].getY());

    }

    public void generateRandomTreePaths () {
        int ht = mapArray.length/3;
        int wt = mapArray[0].length/3;

        CoordinatePair outside_1 = CoordinatePair.randomCoordinatePair(1, wt-1, 1, ht-1);
        CoordinatePair outside_2 = CoordinatePair.randomCoordinatePair(1, wt-1, ht+1, 2*ht-1);
        CoordinatePair outside_3 = CoordinatePair.randomCoordinatePair(1, wt-1, 2*ht+1, 3*ht-1);
        CoordinatePair outside_4 = CoordinatePair.randomCoordinatePair(wt+1, 2*wt-1, 2*ht+1, 3*ht-1);
        CoordinatePair outside_5 = CoordinatePair.randomCoordinatePair(2*wt+1, 3*wt-1, 2*ht+1, 3*ht-1);
        CoordinatePair outside_6 = CoordinatePair.randomCoordinatePair(2*wt+1, 3*wt-1, ht+1, 2*ht-1);
        CoordinatePair outside_7 = CoordinatePair.randomCoordinatePair(2*wt+1, 3*wt-1,1, ht-1);
        CoordinatePair outside_8 = CoordinatePair.randomCoordinatePair(wt+1, 2*wt-1, 1, ht-1);
        CoordinatePair inside = CoordinatePair.randomCoordinatePair(wt+1, 2*wt-1, ht+1, 2*ht-1);
        startPosition = new CoordinatePair(outside_8.getX(), mapArray.length-1);

        addVerticalPath(outside_8.getX(), startPosition.getY(), outside_8.getY());
        randomConnectionStyle(outside_8, inside);
        randomConnectionStyle(inside, outside_4);
        randomConnectionStyle(outside_8, outside_1);
        randomConnectionStyle(outside_8, outside_7);
        randomConnectionStyle(inside, outside_2);
        randomConnectionStyle(inside, outside_6);
        randomConnectionStyle(outside_4, outside_3);
        randomConnectionStyle(outside_4, outside_5);

        int[] connections = new int[Utilities.randInt(1,4)];
        for (int i = 0 ; i < connections.length ; i++) {
            int connection = Utilities.randInt(4);
            while (contains(connections, connection)) {
                connection = Utilities.randInt(4);
            }
            connections[i] = connection;
        }
        for (int i : connections) {
            switch(i) {
                case 0:
                    randomConnectionStyle(outside_1, outside_2);
                    break;
                case 1:
                    randomConnectionStyle(outside_2, outside_3);
                    break;
                case 2:
                    randomConnectionStyle(outside_5, outside_6);
                    break;
                case 3:
                    randomConnectionStyle(outside_6, outside_7);
                    break;
            }
        }
    }

    public boolean contains(int[] array, int integer) {
        for (int i : array) {
            if (i == integer) {
                return true;
            }
        }
        return false;
    }

    public void randomConnectionStyle (CoordinatePair c1, CoordinatePair c2) {
        int rand = Utilities.randInt(1);
        switch (rand) {
            case 0:
                addCornerPathInside(c1.getX(), c1.getY(), c2.getX(), c2.getY());
                break;
            case 1:
                addCornerPathOutside(c1.getX(), c1.getY(), c2.getX(), c2.getY());
        }
    }

    public void setNonPassableTerrainToCaveWall () {
        for (int i = 0 ; i < mapArray.length ; i++) {
            for (int j = 0; j < mapArray[0].length; j++) {
                if (mapArray[i][j] instanceof NonPassableTerrain) {
                    ((NonPassableTerrain) mapArray[i][j]).setTerrainType(TerrainType.CAVEWALL);
                }
            }
        }
    }

    public void randomiseNonPassableTerrainTypes(){
        for (int i = 0 ; i < mapArray.length ; i++){
            for (int j = 0 ; j < mapArray[0].length ; j++) {
                if (mapArray[i][j] instanceof NonPassableTerrain) {
                    if (i == 0 && j == 0) {
                        setNonPassableTerrainTypesWithEqualChances(i, j);
                    } else if (i == 0 && j > 0) {
                        if (mapArray[i][j-1] instanceof PassableTerrain) {
                            setNonPassableTerrainTypesWithEqualChances(i, j);
                        } else {
                            TerrainType reference = ((NonPassableTerrain) mapArray[i][j-1]).getTerrainType();
                            setNonPassableTerrainTypesWithOneReference(i, j, reference);
                        }
                    } else if (i > 0 && j == 0) {
                        if (mapArray[i-1][j] instanceof PassableTerrain) {
                            setNonPassableTerrainTypesWithEqualChances(i, j);
                        } else {
                            TerrainType reference = ((NonPassableTerrain) mapArray[i-1][j]).getTerrainType();
                            setNonPassableTerrainTypesWithOneReference(i, j, reference);
                        }
                    } else {
                        if (mapArray[i][j-1] instanceof PassableTerrain && mapArray[i-1][j] instanceof PassableTerrain) {
                            setNonPassableTerrainTypesWithEqualChances(i, j);
                        } else if (mapArray[i][j-1] instanceof PassableTerrain) {
                            TerrainType reference = ((NonPassableTerrain) mapArray[i-1][j]).getTerrainType();
                            setNonPassableTerrainTypesWithOneReference(i, j, reference);
                        } else if (mapArray[i-1][j] instanceof PassableTerrain) {
                            TerrainType reference = ((NonPassableTerrain) mapArray[i][j-1]).getTerrainType();
                            setNonPassableTerrainTypesWithOneReference(i, j, reference);
                        } else {
                            TerrainType reference_1 = ((NonPassableTerrain) mapArray[i-1][j]).getTerrainType();
                            TerrainType reference_2 = ((NonPassableTerrain) mapArray[i][j-1]).getTerrainType();
                            setNonPassableTerrainTypesWithTwoReferences(i, j, reference_1, reference_2);
                        }
                    }
                }
            }
        }
    }
    public void setNonPassableTerrainTypesWithEqualChances (int i, int j) {
        int typeChoice = Utilities.throwDice(4);
        switch (typeChoice) {
            case 1:
                ((NonPassableTerrain) mapArray[i][j]).setTerrainType(TerrainType.FORREST);
                break;
            case 2:
                ((NonPassableTerrain) mapArray[i][j]).setTerrainType(TerrainType.MARSH);
                break;
            case 3:
                ((NonPassableTerrain) mapArray[i][j]).setTerrainType(TerrainType.MOUNTAIN);
                break;
            case 4:
                ((NonPassableTerrain) mapArray[i][j]).setTerrainType(TerrainType.WATER);
                break;
        }
    }

    public void setNonPassableTerrainTypesWithOneReference (int i, int j, TerrainType reference) {
        int typeChoice = Utilities.throwDice(10);
        if (typeChoice > 0 && typeChoice < 7) {
            ((NonPassableTerrain) mapArray[i][j]).setTerrainType(reference);
        } else {
            setNonPassableTerrainTypesWithEqualChances(i, j);
        }
    }

    public void setNonPassableTerrainTypesWithTwoReferences (int i, int j, TerrainType reference_1, TerrainType reference_2) {
        int typeChoice = Utilities.throwDice(10);
        if (typeChoice > 0 && typeChoice <= 3) {
            ((NonPassableTerrain) mapArray[i][j]).setTerrainType(reference_1);
        } else if (typeChoice > 3 && typeChoice <= 6) {
            ((NonPassableTerrain) mapArray[i][j]).setTerrainType(reference_2);
        } else {
            setNonPassableTerrainTypesWithEqualChances(i, j);
        }
    }

    public void putCreatureOnRoad (Creature creature, int x, int y) {
        if (mapArray[y][x] instanceof PassableTerrain){
            ((PassableTerrain) mapArray[y][x]).setCreature(creature);
            ((PassableTerrain) mapArray[y][x]).setPassed(false);
        } else {
            System.out.println("You cannot put a monster on square " + x + "-" + y);
        }
    }

    public void putCreatureOnRoad (Creature creature, CoordinatePair coordinatePair) {
        int x = coordinatePair.getX();
        int y = coordinatePair.getY();
        if (mapArray[y][x] instanceof PassableTerrain){
            ((PassableTerrain) mapArray[y][x]).setCreature(creature);
            ((PassableTerrain) mapArray[y][x]).setPassed(false);
        } else {
            System.out.println("You cannot put a monster on square " + x + "-" + y);
        }
    }


    public static void generateAndPrintMinimapFromPosition (Map map, int x, int y) {
        Terrain[][] bigMapArr = map.getMapArray();
        Map miniMap = new Map();
        miniMap.initiateEmptyMap(5,5);
        Terrain[][] miniMapArr = miniMap.getMapArray();
        for (int i = 0 ; i < 5 ; i++) {
            for (int j = 0 ; j < 5 ; j++) {
                if ((x-2+j) < 0 || (x-2+j) >= map.getWidth() || (y-2+i) < 0 || (y-2+i) >= map.getHeight()) {
                    miniMapArr[i][j] = new MiniMapSpecialTerrain(TerrainType.INVISIBLE_TERRAIN);
                } else if (!bigMapArr[y-2+i][x-2+j].isPassed()) {
                    miniMapArr[i][j] = new MiniMapSpecialTerrain(TerrainType.LIVING_CREATURE);
                }else if (bigMapArr[y-2+i][x-2+j] instanceof PassableTerrain) {
                    if (((PassableTerrain) bigMapArr[y-2+i][x-2+j]).getCreature() != null) {
                        miniMapArr[i][j] = new MiniMapSpecialTerrain(TerrainType.DEAD_CREATURE);
                    } else {
                        miniMapArr[i][j] = bigMapArr[y-2+i][x-2+j];
                    }
                } else {
                    miniMapArr[i][j] = bigMapArr[y-2+i][x-2+j];
                }
            }
        }
        miniMapArr[2][2] = new MiniMapSpecialTerrain(TerrainType.MY_POSITION);
        miniMapArr[0][0] = new MiniMapSpecialTerrain(TerrainType.INVISIBLE_TERRAIN);
        miniMapArr[0][4] = new MiniMapSpecialTerrain(TerrainType.INVISIBLE_TERRAIN);
        miniMapArr[4][0] = new MiniMapSpecialTerrain(TerrainType.INVISIBLE_TERRAIN);
        miniMapArr[4][4] = new MiniMapSpecialTerrain(TerrainType.INVISIBLE_TERRAIN);
        miniMap.setMapArray(miniMapArr);
        System.out.println("Current view:");
        miniMap.printMapColours();
    }

    public static void printExploredMap(Map map, HashSet<CoordinatePair> pastCoordinates, CoordinatePair currentCoordinates) {
        Terrain[][] bigMapArr = map.getMapArray();
        Map exploredMap = new Map();
        exploredMap.initiateEmptyMap(map.getWidth(),map.getHeight());
        Terrain[][] exploredMapArray = exploredMap.getMapArray();
        for (CoordinatePair coPair : pastCoordinates) {
            int x = coPair.getX();
            int y = coPair.getY();
            for (int i = -1 ; i <= 1 ; i++) {
                for (int j = -1; j <= 1 ; j++) {
                    if (checkIfCoordinateIsOnMap(map, x+j, y+i)) {
                        exploredMapArray[y+i][x+j] = bigMapArr[y+i][x+j];
                    }
                }
            }
            for (int i = -2 ; i <= 2 ; i+=4) {
                for (int j = -1 ; j <= 1 ; j++) {
                    if (checkIfCoordinateIsOnMap(map, x+j, y+i)) {
                        exploredMapArray[y+i][x+j] = bigMapArr[y+i][x+j];
                    }
                }
            }
            for(int i = -1 ; i <= 1 ; i++) {
                for (int j = -2 ; j <= 2 ; j+=4) {
                    if (checkIfCoordinateIsOnMap(map, x+j, y+i)) {
                        exploredMapArray[y+i][x+j] = bigMapArr[y+i][x+j];
                    }
                }
            }
            exploredMapArray[currentCoordinates.getY()][currentCoordinates.getX()] = new MiniMapSpecialTerrain(TerrainType.MY_POSITION);
        }
        exploredMap.setMapArray(exploredMapArray);
        exploredMap.printMapColours();
    }

    public static boolean checkIfCoordinateIsOnMap (Map map, int x, int y){
        Terrain[][] mapArr = map.getMapArray();
        boolean coordinateOnMap = true;
        int xMax = mapArr[0].length;
        int yMax = mapArr.length;
        if (y >= yMax || y < 0 || x >= xMax || x < 0) {
            coordinateOnMap = false;
        }
        return coordinateOnMap;
    }
    public static boolean checkIfCoordinateIsOnMap (Map map, CoordinatePair coPair){
        Terrain[][] mapArr = map.getMapArray();
        boolean coordinateOnMap = true;
        int xMax = mapArr[0].length;
        int yMax = mapArr.length;
        int x = coPair.getX();
        int y = coPair.getY();
        if (y >= yMax || y < 0 || x >= xMax || x < 0) {
            coordinateOnMap = false;
        }
        return coordinateOnMap;
    }

    public void printMap() {
        for (int i = 0 ; i < mapArray.length ; i++) {
            for (int j = 0; j < mapArray[i].length; j++) {
                System.out.print(mapArray[i][j].getVisual() + "  ");
            }
            System.out.print("\n");
        }
    }
    public void printMapColours() {
        for (int i = 0 ; i < mapArray.length ; i++) {
            for (int j = 0 ; j < mapArray[i].length ; j++) {
                if (mapArray[i][j] instanceof MiniMapSpecialTerrain){
                    System.out.print(mapArray[i][j].getVisual() + "  ");
                } else if(mapArray[i][j] instanceof PassableTerrain) {
                    System.out.print(ANSI_RED + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                } else if (((NonPassableTerrain)mapArray[i][j]).getTerrainType() == TerrainType.WATER){
                    System.out.print(ANSI_BLUE + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                }else if (((NonPassableTerrain)mapArray[i][j]).getTerrainType() == TerrainType.FORREST){
                    System.out.print(ANSI_GREEN + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                }else if (((NonPassableTerrain)mapArray[i][j]).getTerrainType() == TerrainType.MOUNTAIN){
                    System.out.print(ANSI_BLACK + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                }else if (((NonPassableTerrain)mapArray[i][j]).getTerrainType() == TerrainType.MARSH){
                    System.out.print(ANSI_YELLOW + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                } else if (((NonPassableTerrain)mapArray[i][j]).getTerrainType() == TerrainType.CAVEWALL) {
                    System.out.print(ANSI_CYAN + mapArray[i][j].getVisual() + "  " + ANSI_RESET);
                } else {
                    System.out.print(mapArray[i][j].getVisual() + "  ");
                }
            }
            System.out.print("\n");
        }
    }

    public static void printMapLegend() {
        System.out.println("----- Map Legend -----");
        System.out.println(ANSI_RED + "X" + ANSI_RESET + ": Path");
        System.out.println(ANSI_BLUE + "~" + ANSI_RESET + ": Water");
        System.out.println(ANSI_YELLOW + "_" + ANSI_RESET + ": Marshlands");
        System.out.println(ANSI_GREEN + "&" + ANSI_RESET + ": Dense forest");
        System.out.println(ANSI_BLACK + "M" + ANSI_RESET + ": Mountainous terrain");
        System.out.println(ANSI_CYAN + "/" + ANSI_RESET + ": Cave wall");
        System.out.println("o: Player position");
        System.out.println("x: Enemy creature");
        System.out.println("✝: Corpse of dead enemy");
        System.out.println();
    }



    public Terrain[][] getMapArray() {
        return mapArray;
    }

    public void setMapArray(Terrain[][] mapArray) {
        this.mapArray = mapArray;
    }

    public CoordinatePair getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(CoordinatePair startPosition) {
        this.startPosition = startPosition;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName(){
        return name;
    }
}
