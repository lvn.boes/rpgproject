package be.daStudios.map;

import java.io.Serializable;

public class MiniMapSpecialTerrain implements Terrain, Serializable {
    private char visual = '.';
    private TerrainType specialTerrainType;


    MiniMapSpecialTerrain (TerrainType tt) {
        setTerrainType(tt);
    }

    public char getVisual() {
        return visual;
    }

    public void setVisual(char visual) {
        this.visual = visual;
    }

    @Override
    public boolean isPassed() {
        return true;
    }

    public TerrainType getTerrainType() {
        return specialTerrainType;
    }

    public void setTerrainType(TerrainType specialTerrainType) {
        this.specialTerrainType = specialTerrainType;
        setVisual(specialTerrainType.getVisual());
    }
}
