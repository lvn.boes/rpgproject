package be.daStudios.map;

import be.daStudios.creatures.Creature;

import java.io.Serializable;

public class PassableTerrain implements Terrain, Serializable {
    private char visual = 'X';
    Creature creature = null;
    boolean passed=true;
    boolean lockedGate = false;



    public char getVisual() {
        return visual;
    }

    public void setVisual(char visual) {
        this.visual = visual;
    }

    public Creature getCreature() {
        return creature;
    }

    public void setCreature(Creature creature) {
        this.creature = creature;
        passed=false;
    }

    @Override
    public boolean isPassed(){
        return this.passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public boolean isLockedGate() {
        return lockedGate;
    }

    public void setLockedGate(boolean lockedGate) {
        this.lockedGate = lockedGate;
    }

    @Override
    public String toString() {
        return "PassableTerrain{" +
                "visual=" + visual +
                ", creature=" + creature +
                ", passed=" + passed +
                ", lockedGate=" + lockedGate +
                '}';
    }
}
