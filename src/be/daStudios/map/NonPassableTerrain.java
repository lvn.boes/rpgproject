package be.daStudios.map;

import java.io.Serializable;

public class NonPassableTerrain implements Terrain, Serializable {
    private char visual = '.';
    private TerrainType nonPassableType;
    private boolean passed=true;

    public char getVisual() {
        return visual;
    }

    public void setVisual(char visual) {
        this.visual = visual;
    }

    public TerrainType getTerrainType() {
        return nonPassableType;
    }

    public void setTerrainType(TerrainType nonPassableType) {
        this.nonPassableType = nonPassableType;
        setVisual(nonPassableType.getVisual());
    }

    @Override
    public boolean isPassed() {
        return passed;
    }

}
