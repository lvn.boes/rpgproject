package be.daStudios.map;

import java.io.Serializable;

public interface Terrain extends Serializable {
    char getVisual ();
    void setVisual (char visual);
    boolean isPassed();
}
