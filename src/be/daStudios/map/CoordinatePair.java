package be.daStudios.map;

import be.daStudios.utilities.Utilities;

import java.io.Serializable;
import java.util.Objects;

public class CoordinatePair implements Serializable {
    private int X;
    private int Y;

    public CoordinatePair (int x, int y) {
        setX(x);
        setY(y);
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        Y = y;
    }

    public static CoordinatePair randomCoordinatePair(int xMin, int xMax, int yMin, int yMax) {
        int X = Utilities.randInt(xMin, xMax);
        int Y = Utilities.randInt(yMin, yMax);
        CoordinatePair cp = new CoordinatePair(X, Y);
        return cp;
    }

    public static CoordinatePair set(int x, int y){
        CoordinatePair cp =new CoordinatePair(x,y);
        return cp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinatePair that = (CoordinatePair) o;
        return X == that.X && Y == that.Y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }
}
