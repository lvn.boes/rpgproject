package be.daStudios.backpack;

import be.daStudios.equippable.Storable;

import java.util.ArrayList;
import java.util.List;

public class BackPack implements Pack {
    private final int MAX_ITEMS=20;
    private int goldPieces;
    private final List<Storable> backpackItems=new ArrayList<>();

    @Override
    public void addItem(Storable item) {
        if(backpackItems.size()<MAX_ITEMS){
            backpackItems.add(item);
        }else {
            System.out.println("Backpack is full, you can't pick up the item");
        }
    }

    @Override
    public Storable switchItem(Storable item,int index) {
        Storable temp=backpackItems.get(index);
        backpackItems.remove(index);
        backpackItems.add(item);
        return temp;
    }

    @Override
    public void removeItem(Storable item) {
        if(backpackItems.remove(item)){
            System.out.println("Item removed successfully");
        }else {
            System.out.println("There is no such item in your backpack");
        }
    }

    @Override
    public void addGold(int amount) {
        if(goldPieces<=500) {
            goldPieces += amount;
        }else {
            System.out.println("You can't put more then 500 coins in the backpack");
        }
    }

    @Override
    public int spendGold(int amount) {
        if(goldPieces>=amount) {
            goldPieces -= amount;
            return amount;
        }else{
            System.out.println("You don't have enough coins to spend.");
            return 0;
        }
    }

    @Override
    public String getPackContent() {
        if(backpackItems.size()>0) {
            StringBuilder sb = new StringBuilder("Content of Pack:\n");
            for (int i=0; i<backpackItems.size();i++) {
                sb.append(i+1).append(". ").append(backpackItems.get(i).toString()).append("\n");
            }
            sb.append("GP: ").append(goldPieces).append("\n");
            return sb.toString();
        }else {
            return "The backpack is empty";
        }
    }

    @Override
    public Storable getItem(int index) {
        return backpackItems.get(index);
    }

    @Override
    public int size() {
        return backpackItems.size();
    }

    @Override
    public int getGoldPieces() {
        return goldPieces;
    }

    @Override
    public void setGoldPieces(int goldPieces) {
        this.goldPieces = goldPieces;
    }
}
