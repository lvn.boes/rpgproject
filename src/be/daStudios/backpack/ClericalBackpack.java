package be.daStudios.backpack;

import be.daStudios.equippable.Storable;
import be.daStudios.equippable.spellBooks.Books;

import java.util.ArrayList;
import java.util.List;

public class ClericalBackpack implements Pack {
    private final int MAX_ITEMS=15;
    private int goldPieces;
    private final List<Storable> backpackItems=new ArrayList<>();
    private final List<Books> spellBookList=new ArrayList<>();

    public void addSpellBook(Books spellBook){
        if(spellBookList.size()<=5){
        spellBookList.add(spellBook);
        }else {
            System.out.println("You have already five books in your bag.");
        }
    }
    public void removeSpellbook(Books spellBook){
        if(spellBookList.size()>0){
            spellBookList.remove(spellBook);
        }else {
            System.out.println("There aren't any books in your backpack.");
        }
    }

    @Override
    public int size() {
        return backpackItems.size();
    }
    public int spellBookSize(){
        return spellBookList.size();
    }

    @Override
    public Storable getItem(int index) {
        return backpackItems.get(index);
    }

    public Storable getBook(int index){
        return spellBookList.get(index);
    }

    @Override
    public void addItem(Storable item) {
        if(backpackItems.size()<MAX_ITEMS){
            backpackItems.add(item);
        }else {
            System.out.println("Backpack is full, you can't pick up the item");
        }
    }

    @Override
    public Storable switchItem(Storable item, int index) {
        Storable temp;
        if(item instanceof Books){
            temp= spellBookList.get(index);
            spellBookList.remove(index);
            spellBookList.add((Books) item);
        }else {
            temp = backpackItems.get(index);
            backpackItems.remove(index);
            backpackItems.add(item);
        }
        return temp;
    }

    @Override
    public void removeItem(Storable item) {
        if(backpackItems.remove(item)){
            System.out.println("Item removed successfully");
        }else {
            System.out.println("There is no such item in your backpack");
        }
    }

    @Override
    public void addGold(int amount) {
        if(goldPieces<=500) {
            goldPieces += amount;
        }else {
            System.out.println("You can't put more then 500 coins in the backpack");
        }
    }

    @Override
    public int spendGold(int amount) {
        if(goldPieces>=amount) {
            goldPieces -= amount;
            return amount;
        }else{
            System.out.println("You don't have enough coins to spend.");
            return 0;
        }
    }

    @Override
    public String getPackContent() {
        StringBuilder sb = new StringBuilder("Content of backpack:\n");
        if(backpackItems.size()>0) {
            for (Storable item : backpackItems) {
                sb.append(item.toString()).append("\n");
            }
        }else {
                sb.append("You haven't got any items\n");
        }
//        if(spellBookList.size()>0) {
//            for (Books book : spellBookList) {
//                sb.append(book.toString()).append("\n");
//            }
//        }else {
//            sb.append("You haven't got any spell books\n");
//        }
        return sb.toString();
    }

    @Override
    public int getGoldPieces() {
        return goldPieces;
    }

    @Override
    public void setGoldPieces(int goldPieces) {
        this.goldPieces = goldPieces;
    }
}
