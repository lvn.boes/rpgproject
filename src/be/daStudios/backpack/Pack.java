package be.daStudios.backpack;

import be.daStudios.equippable.Storable;

import java.io.Serializable;

public interface Pack extends Serializable {
    Storable getItem(int index);
    void addItem(Storable item);
    void removeItem(Storable item);
    void addGold(int amount);
    int spendGold(int amount);
    String getPackContent();
    int getGoldPieces();
    void setGoldPieces(int goldPieces);
    Storable switchItem(Storable item, int index);
    int size();
}
