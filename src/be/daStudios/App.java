package be.daStudios;

import be.daStudios.account.Account;
import be.daStudios.menu.Menu;
import be.daStudios.menu.TextMenus;
import be.daStudios.saveGame.FileSaveGame;
import be.daStudios.utilities.Keyboard;

import java.nio.file.attribute.AttributeView;

public class App {
    public static void main(String[] args) {
        try {
            boolean run = true;
            Account account;
            FileSaveGame fileSaveGame = new FileSaveGame();
            FileSaveGame.createFilePath();
            System.out.println(TextMenus.loadMenu());
            fileSaveGame.loadSavedAccounts();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (run) {
                String inputString = Keyboard.readString(TextMenus.loginMenu());
                switch (inputString) {

                    case "New":

                        String userName = Keyboard.readString("Enter your new user name");
                        String userPassword = Keyboard.readString("Enter your new password");
                        account = new Account(userName, userPassword);
                        fileSaveGame.addAccount(account);
                        Menu.runMenu(account);


                        break;

                    case "Load":
                        boolean runLoad = true;

                        System.out.println(fileSaveGame.contentSavedAccounts());

                        if (!fileSaveGame.isEmpty()) {
                            while (runLoad) {
                                int choice = Keyboard.readInt("Please choose an account\n" +
                                        "Enter 0 to exit", 0, fileSaveGame.getlistSize());
                                int tries = 0;

                                if (choice > 0) {
                                    while (tries < 3) {
                                        String password = Keyboard.readString("Enter password:\n");
                                        if (password.equals(fileSaveGame.getAccount(choice).getUserPassword())) {
                                            System.out.println("\naccess granted\n");
                                            Menu.runMenu(fileSaveGame.getAccount(choice));
                                            runLoad = false;
                                            break;
                                        } else if (password.equals("Quit")) {
                                            runLoad = false;
                                            break;
                                        } else {
                                            System.err.println("Wrong Password");
                                            tries++;
                                        }
                                    }
                                    if (tries == 3) {
                                        System.out.println("you've entered wrong password 3 times, you have to wait");
                                        try {
                                            Thread.sleep(30000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                if (choice == 0) {
                                    runLoad = false;
                                } else {
                                    System.out.println("Invalid input");
                                }
                            }
                        }

                        break;

                    case "Quit":
                        run = false;
                        break;
                    default:
                        System.out.println("invalid input");
                }
            }
            FileSaveGame.saveGame();
            System.out.println(
                    "++++++++++++++++++++++++++\n" +
                            "++  Legend of the Lamb  ++\n" +
                            "++++++++++++++++++++++++++\n" +
                            "++ HOPE TO SEE YOU SOON ++\n" +
                            "++++++++++++++++++++++++++\n");
        } catch (Exception ex) {}
    }
}
