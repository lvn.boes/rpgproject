package be.daStudios.menu;

import java.time.LocalDate;

public class TextMenus {
    public static String loadMenu(){
        return "++++++++++++++++++++++++++\n" +
                "++  Legendof the Lamb  ++\n" +
                "++++++++++++++++++++++++++\n" +
                "-- DA Studios Belgium  --\n" +
                "-------------------------- ";
    }

    public static String mainMenu(){
        return "what do you want to do?\n"+
                "New            - Start a New Game\n" +
                "Load           - Load a saved game\n" +
                "Reset          - Reset all Saved Files\n" +
                "Controls       - Game controls\n" +
                "Settings       - Game Setting\n" +
                "Quit           - Quit game\n";
    }
    public static String raceMenu(){
        return "Waht Race do you want to play with?\n"+
                "Human\n"+
                "Dwarf\n"+
                "Elf\n";
    }
    public static String ClassMenu(){
        return "What Class od you want to play with\n"+
                "Fighter\n"+
                "Healer\n"+
                "Ranger\n";

    }
    public static String charactrersName(){
        return "What is your Characters name?\n"+
                "Your character is begin Created.\n"+
                "Map is begin loaded... \n";
    }
    public static String controls(){
        return
                "New : start a new game/account\n"    +
                "Load : loading the game/account that you saved\n"+
                "Direction: \n" +
                "- Choose z or w to move forward\n"+
                "- Choose s to move back\n"+
                "- Choose q or a to move to the left\n"+
                "- Choose d to move to the right\n"+
                "Fight : attack the monster\n"+
                "Loot : pickup items\n"  +
                "Controls : display controls\n"+
                "Legend : make the map legend\n"  +
                "Map :  the map of the game\n"+
                "Reset : delete your game\n"+
                "Settings : you can change your name and your password\n "+
                "Quit : back to main Menu \n";


    }
    public static String settingsMenu(String user,String password){
        return String.format(
                "UserName : %s %n" +
                "Password : %s %n"+
                "To change your setting %n " +
                "Type %n " +
                "key value %n"+
                "To go back to main menu %n " +
                "Type %n " +
                "Menu %n",
                user,
                password);

    }
    public static String loginMenu(){
        return "New - Create new account\n" +
                "Load - Load an existing account\n" +
                "Quit - Exit\n";
    }

}
