package be.daStudios.menu;

import be.daStudios.account.Account;
import be.daStudios.account.Game;
import be.daStudios.account.InGame;
import be.daStudios.map.Map;
import be.daStudios.saveGame.FileSaveGame;
import be.daStudios.utilities.Keyboard;

public class Menu {

    public static void runMenu(Account account) {
        Account currentAccount=account;
        Game currentGame;
        boolean run=true;
        String inputString;
        int inputInt;
        InGame inGame;

        while (run){

            inputString= Keyboard.readString(TextMenus.mainMenu());

            switch (inputString){
                case "New":
                    currentGame=CreateGame.createGame();
                    inGame=new InGame(currentGame);
                    currentAccount.addGame(currentGame);

                    inGame.runGame();
                    FileSaveGame.saveGame();

                    break;

                case "Load":
                    System.out.println("0. Quit");
                    System.out.println(currentAccount.contentSavedGames());

                    while (true) {
                        inputInt = Keyboard.readInt("enter number",0,account.getListSize());
                        if (inputInt == 0){
                            break;
                        }else if(currentAccount.getGame(inputInt)!=null){
                            System.out.println("loading the game");
                            currentGame=currentAccount.getGame(inputInt);
                            inGame=new InGame(currentGame);
                            inGame.runGame();
                            FileSaveGame.saveGame();
                            break;
                        }else {
                            System.out.println("invalid input");
                        }
                    }

                    break;

                case "Reset":
                    System.out.println(currentAccount.contentSavedGames());
                    System.out.println("0. Quit");
                    while (true) {
                        inputInt = Keyboard.readInt("enter number",0,account.getListSize());
                        if (inputInt == 0){
                            break;
                        }else if(currentAccount.getGame(inputInt)!=null){
                            account.removeGame(inputInt);
                            System.out.println("Game deleted");
                            break;
                        }else {
                            System.out.println("invalid input");
                        }
                    }
                    break;

                case "Controls":
                    Map.printMapLegend();
                    System.out.println();
                    System.out.println(TextMenus.controls());
                    break;

                case "Settings":
                    boolean runSettings=true;
                    System.out.println(TextMenus.settingsMenu(account.getUserName(),account.getSecretPassword()));
                    while (runSettings){
                        inputString=Keyboard.readString("choice");
                        switch (inputString){
                            case "Name":
                                while (true) {
                                    String name = Keyboard.readString("Enter your new name: ");
                                    if (Keyboard.validateYN()) {
                                        account.setUserName(name);
                                        System.out.println(TextMenus.settingsMenu(account.getUserName(),account.getSecretPassword()));
                                        break;
                                    }
                                }
                                break;
                            case "Password":
                                while (true) {
                                    String password = Keyboard.readString("Enter your new password: ");
                                    if (Keyboard.validateYN()) {
                                        account.setUserPassword(password);
                                        System.out.println(TextMenus.settingsMenu(account.getUserName(),account.getSecretPassword()));
                                        break;
                                    }
                                }

                            case "Menu":
                                runSettings=false;
                                break;
                        }
                    }

                    break;

                case "Quit":
                    run = false;
                    FileSaveGame.saveGame();


                    break;

                default:
                    System.out.println("Wrong input");


            }
        }
    }
}
