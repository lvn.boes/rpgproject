package be.daStudios.menu;

import be.daStudios.account.Game;
import be.daStudios.creatures.*;
import be.daStudios.map.Map;
import be.daStudios.utilities.Keyboard;

public class CreateGame {
    public static Game createGame(){
        return new Game(newPlayer(),newMap(),newName());
    }

    private static Map newMap(){
        System.out.println("What map do you want to play?\n" +
                "1. Forest of Streams\n" +
                "2. Cave of Thread\n" +
                "3. Random outside\n" +
                "4. Random cave\n" +
                "5. Full random"
        );
        Map map= new Map();
        int choice= Keyboard.readInt("number:");
        switch (choice){
            case 1:
                map.initiateForestOfStreams();
            break;
            case 2:
                map.initiateCaveOfThreads();
            break;
            case 3:
                map.initiateRandomAboveGroundMap();
            break;
            case 4:
                map.initiateRandomUndergroundMap();
            break;
            case 5:
                map.initiateFullRandomMap();
            break;
            default:
                System.out.println("Invalid choice");
        }
        return map;
    }
    private static Race newRace(){
        System.out.println("What Race do you want to play with?\n" +
                "Human\n" +
                "Dwarf\n" +
                "Elf"
        );
        while (true){
            String choice=Keyboard.readString("choice:");
            switch (choice) {
                case "Human":
                    return Race.Human;
                case "Dwarf":
                    return Race.Dwarf;
                case "Elf":
                    return Race.Elf;
                default:
                    System.out.println("Wrong choice");
            }
        }
    }

    private static PlayableCreature newPlayer(){
        Race race=newRace();
        System.out.println("What class do you want to play with?\n" +
                "Fighter\n" +
                "Healer\n" +
                "Ranger");
        while (true) {
            String choice = Keyboard.readString("choice:");
            switch (choice) {
                case "Fighter":
                    return new Fighter(race);
                case "Healer":
                    return new Healer(race);
                case "Ranger":
                    return new Ranger(race);
                default:
                    System.out.println("Wrong choice");
            }
        }
    }
    private static String newName(){
        boolean save;
        String name;
        do{
            name=Keyboard.readString("Enter your name:");
            System.out.printf("Do you want to save '%s' as your name? %n",name);
            save=Keyboard.validateYN();
        }while (!save);
        return name;
    }
}
